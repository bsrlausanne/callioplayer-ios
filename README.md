## CallioPlayer iOS

Cette application iOS a �t� develop�e en Swift 2 avec XCode sur mac.

La description du produit final sur l'[appstore](https://itunes.apple.com/fr/app/callioplayer/id1142580992?mt=8).

Il s'agit en bref d'un lecteur audio sp�cialis� ([format DAISY](http://www.daisy.org/daisypedia/daisy-digital-talking-book)) avec acc�s authentifi� � [notre collection de livres audio](http://www.bibliothequesonore.ch).
Il a �t� r�alis� par des d�butants en programmation iOS.

Le code est disponible ici pour d'autres institutions qui souhaiteraient faire quelque chose de comparable et qui auraient l'utilit� d'un exemple fonctionnel.
D�pendances:

[Alamofire](https://github.com/Alamofire/Alamofire) pour les t�l�chargements

[Reactive Cocoa](https://github.com/ReactiveCocoa/ReactiveCocoa) 

[Realm](https://realm.io/) pour la permanence des donn�es

L'application se connecte � notre webservice qui r�cup�re les donn�es de notre biblioth�que. Son code n'est pas public.
La base de donn�es des livres utilise Apache Solr, et le webservice transmet les requ�tes directement.

## Auteurs

St�phane Maillard
Simon Schul� sschule@bibliothequesonore.ch.
