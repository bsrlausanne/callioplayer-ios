//
//  CatalogAudiodescriptionDataSource.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 10.05.23.
//  Copyright © 2023 Bibliothèque Sonore Romande. All rights reserved.
//


import UIKit
import ReactiveSwift

class CatalogAudiodescriptionDataSource: TableViewDataSource {
    
    typealias T = NewSearchResponse
    
    var navigationTitle: String = "Audiodescription"
    
    var downloadBooksCallback: (PaginationParameters) -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
        return { params in
            let values = """
            {"genreCode":"AU AUT", "page":"\(params.page)"}
            """
            return ApiHandler.bookSearch(values)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, viewController: Controller<NewSearchResponse>) -> UITableViewCell {
        let options = BuildTableViewOptions(
            books: viewController.books,
            cellIdentifier: "CatalogAudiodescriptionViewCell",
            readSampleCallback: viewController.readSample,
            downloadBookCallback: viewController.downloadPressed,
            bookmarkBookCallback: viewController.bookmarkPressed
        )
        
        return buildTableView(tableView, cellForRowAt: indexPath, options: options, CatalogAudiodescriptionViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int, viewController: Controller<NewSearchResponse>) -> Int {
        return viewController.books.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, viewController: Controller<NewSearchResponse>) {
        DispatchQueue.main.async { () -> Void in
            viewController.performSegue(withIdentifier: "ShowBookFromList", sender: tableView.cellForRow(at: indexPath))
        }
    }
}
