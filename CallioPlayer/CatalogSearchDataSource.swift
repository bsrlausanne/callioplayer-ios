//
//  CatalogNewBooksDataSource.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 02.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit
import ReactiveSwift

class CatalogSearchDataSource: TableViewDataSource {
    typealias T = NewSearchResponse
    private var query: NewSearchParameters
    
    var navigationTitle: String = "Résultats"
    
    init(query: NewSearchParameters) {
        self.query = query
    }
    
    var downloadBooksCallback: (PaginationParameters) -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
        return { params in
            let p = self.query.withPagination(params)
            
            return ApiHandler.bookSearch(p.asString())
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, viewController: Controller<NewSearchResponse>) -> UITableViewCell {
        let options = BuildTableViewOptions(
            books: viewController.books,
            cellIdentifier: "CatalogSearchViewCell",
            // currentPageNb: Int,
            readSampleCallback: viewController.readSample,
            downloadBookCallback: viewController.downloadPressed,
            bookmarkBookCallback: viewController.bookmarkPressed
        )
        
        return buildTableView(tableView, cellForRowAt: indexPath, options: options, CatalogSearchViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int, viewController: Controller<NewSearchResponse>) -> Int {
        return viewController.books.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, viewController: Controller<NewSearchResponse>) {
        DispatchQueue.main.async { () -> Void in
            viewController.performSegue(withIdentifier: "ShowBookFromList", sender: tableView.cellForRow(at: indexPath))
        }
    }
}
