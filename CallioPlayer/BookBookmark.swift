//
//  BookBookmark.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 26.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct BookBookmark {
    var chapterId = ""
    var startPoint = 0
    var duration = 0
    var title = ""
    
    init () {
        
    }
    
    init (bookmarkDB: BookmarkDB) {
        self.chapterId = bookmarkDB.chapterId
        self.startPoint = bookmarkDB.startPoint
        self.duration = bookmarkDB.duration
        self.title = bookmarkDB.title
        
    }
    
    func toDB() -> BookmarkDB {
        let bookmarkDB = BookmarkDB()
        
        bookmarkDB.chapterId = self.chapterId
        bookmarkDB.startPoint = self.startPoint
        bookmarkDB.duration = self.duration
        bookmarkDB.title = self.title
        
        return bookmarkDB
    }
}
