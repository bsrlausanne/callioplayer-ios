//
//  Setting.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 27.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

struct Setting {
    let label: String
    let imageName: String
    let controlElement: UISwitch?
    let pressedCallback: (() -> Void)?
    
    init(
        label: String,
        imageName: String,
        controlElement: UISwitch?,
        pressedCallback: (() -> Void)?
    ) {
        self.label = label
        self.imageName = imageName
        self.controlElement = controlElement
        self.pressedCallback = pressedCallback
    }
    
    init(
        label: String,
        imageName: String,
        controlElement: UISwitch
    ) {
        self.init(
            label: label,
            imageName: imageName,
            controlElement: controlElement,
            pressedCallback: nil
        )
    }
    
    init(
        label: String,
        imageName: String,
        pressedCallback: @escaping () -> Void
    ) {
        self.init(
            label: label,
            imageName: imageName,
            controlElement: nil,
            pressedCallback: pressedCallback
        )
    }
}
