//
//  CatalogSearchViewCell.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 14.07.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit

class CatalogSearchViewCell: BookListViewCell {
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        toolbar.accessibilityElementsHidden = true
        // Configure the view for the selected state
    }
    
}
