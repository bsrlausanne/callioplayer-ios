//
//  BookReaderInteractor.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 25.05.16.
//  Copyright (c) 2016 Bibliothèque Sonore Romande. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit

protocol BookReaderInteractorInput {
  func doSomething(request: BookReaderRequest)
}

protocol BookReaderInteractorOutput {
  func presentSomething(response: BookReaderResponse)
}

class BookReaderInteractor: BookReaderInteractorInput {
  var output: BookReaderInteractorOutput!
  var worker: BookReaderWorker!
  
  // MARK: Business logic
  
  func doSomething(request: BookReaderRequest) {
    // NOTE: Create some Worker to do the work
    
    worker = BookReaderWorker()
    worker.doSomeWork()
    
    // NOTE: Pass the result to the Presenter
    
    let response = BookReaderResponse()
    output.presentSomething(response)
  }
}
