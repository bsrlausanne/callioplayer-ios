//
//  CatalogFreeBooksDataSource.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 29.03.22.
//  Copyright © 2022 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit
import ReactiveSwift

class CatalogFreeBooksDataSource: TableViewDataSource {
    
    typealias T = GetFreeBooksResponse
    
    var navigationTitle: String = "Livres d'essai"
    
    var downloadBooksCallback: (PaginationParameters) -> SignalProducer<WebServiceResponse<GetFreeBooksResponse>, NSError> {
        return { _ in
            return ApiHandler.getFreeBooks()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, viewController: Controller<GetFreeBooksResponse>) -> UITableViewCell {
        let options = BuildTableViewOptions(
            books: viewController.books,
            cellIdentifier: "CatalogFreeBooksViewCell",
            readSampleCallback: viewController.readSample,
            downloadBookCallback: viewController.downloadPressed,
            bookmarkBookCallback: viewController.bookmarkPressed
        )
        
        return buildTableView(tableView, cellForRowAt: indexPath, options: options, CatalogFreeBooksViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int, viewController: Controller<GetFreeBooksResponse>) -> Int {
        return viewController.books.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, viewController: Controller<GetFreeBooksResponse>) {
        DispatchQueue.main.async { () -> Void in
            viewController.performSegue(withIdentifier: "ShowBookFromList", sender: tableView.cellForRow(at: indexPath))
        }
    }
    
}
