//
//  CatalogWishListViewCell.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 07.06.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit

class CatalogWishListViewCell: BookListViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        log.debug("wishlist awake!")
    }
}
