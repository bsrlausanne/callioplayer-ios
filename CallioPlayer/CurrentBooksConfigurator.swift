//
//  CurrentBooksConfigurator.swift
//  CallioPlayer
//
//  Created by Stéphane Maillard on 20.05.16.
//  Copyright (c) 2016 Bibliothèque Sonore Romande. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so you can apply
//  clean architecture to your iOS and Mac projects, see http://clean-swift.com
//

import UIKit


class CurrentBooksConfigurator {
    static let sharedInstance = CurrentBooksConfigurator()
    
    // MARK: Configuration
    
    func configure(_ viewController: CurrentBooksViewController) {
        let presenter = CurrentBooksPresenter()
        presenter.view = viewController
        
        viewController.presenter = presenter
    }
}
