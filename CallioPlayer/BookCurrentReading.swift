//
//  BookCurrentReading.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 26.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct BookCurrentReading {
    var isReading = false
    var lastRead = Date()
    var chapterId = ""
    var startPoint = 0
    
    init () {
        
    }
    
    init (currentReadingDB: CurrentReadingDB) {
        self.chapterId = currentReadingDB.chapterId
        self.isReading = currentReadingDB.isReading
        self.startPoint = currentReadingDB.startPoint
    }
    
    func toDB() -> CurrentReadingDB {
        let currentReadingDB = CurrentReadingDB()
        
        currentReadingDB.chapterId = self.chapterId
        currentReadingDB.isReading = self.isReading
        currentReadingDB.startPoint = self.startPoint
        
        return currentReadingDB
    }
}
