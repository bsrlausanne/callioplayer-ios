//
//  BuildTableViewOptions.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 04.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit

struct BuildTableViewOptions {
    var books: [BookModel]
    var cellIdentifier: String
    var readSampleCallback: (UIBarButtonItem) -> Void
    var downloadBookCallback: (UIBarButtonItem) -> Void
    var bookmarkBookCallback: (UIBarButtonItem) -> Void
}
