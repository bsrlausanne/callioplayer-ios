//
//  Presenter.swift
//  CallioPlayer
//
//  Created by Faton Ramadani on 28.02.17.
//  Copyright © 2017 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift

class Presenter<T: KeyDecodable & Countable> {
    private var view: Controller<T>!
    func setView(_ view: Controller<T>) {
        log.debug("Setting view")
        log.debug(view)
        self.view = view
    }
    
    func getView() -> Controller<T>? {
        log.debug("Getting view")
        log.debug(self.view)
        return self.view
    }
    
    private var paginationParams = PaginationParameters()
    var pageNb: Int {
        get {
            return paginationParams.page
        }
    }
    private var availableBooks : Array<BookModel> = Array()

    private var savedCallback: ((PaginationParameters) -> SignalProducer<WebServiceResponse<T>, NSError>)?

    required init() {}
    
    // TODO: Make this an extension function ?
    func downloadBooks(
        _ queryCallback: @escaping (PaginationParameters) -> SignalProducer<WebServiceResponse<T>, NSError>,
        prevButton: UIButton,
        nextButton : UIButton,
        pageController: UIView,
        nbPages: UILabel
    ) {
        log.debug("Saving callback")
        savedCallback = queryCallback

        queryCallback(paginationParams).observe(
            on: UIScheduler()
        ).startWithResult { result in
            switch result {
            case let .success(response):
                self.updateTableUI(
                    result: response.result,
                    prevButton: prevButton,
                    nextButton: nextButton,
                    pageController: pageController,
                    nbPages: nbPages
                )
            case let .failure(error):
                log.debug(error)
            }
        }
    }

    func showNext(
        nextButton: UIButton,
        prevButton: UIButton,
        pageController: UIView,
        postLoadCallback cb: @escaping () -> Void = {},
        nbPages: UILabel
    ) {
        log.debug(savedCallback)
        paginationParams.page += 1

        savedCallback!(paginationParams).observe(
            on: UIScheduler()
        ).startWithResult { result in
            switch result {
            case let .success(response):
                self.updateTableUI(
                    result: response.result,
                    prevButton: prevButton,
                    nextButton: nextButton,
                    pageController: pageController,
                    nbPages: nbPages
                )

                cb()
            case let .failure(error):
                log.debug(error)
            }
        }
    }
    
    func showPrev(
        nextButton: UIButton,
        prevButton: UIButton,
        pageController: UIView,
        postLoadCallback cb: @escaping () -> Void = {},
        nbPages: UILabel
    ) -> Void {
        log.debug(savedCallback)
        paginationParams.page -= 1
        
        savedCallback!(paginationParams).observe(
            on: UIScheduler()
        ).startWithResult { result in
            switch result {
            case let .success(response):
                self.updateTableUI(
                    result: response.result,
                    prevButton: prevButton,
                    nextButton: nextButton,
                    pageController: pageController,
                    nbPages: nbPages
                )
                cb()
            case let .failure(error):
                log.debug(error)
            }
        }

        if (paginationParams.page == 0) {
            prevButton.isHidden = true;
        }
        nextButton.isHidden = false
    }
    
    
    func emptyBook() {
        availableBooks = [];
    }
    
    func addToWishList(_ book: BookModel ) {
        ApiHandler.addWish(book).observe(
            on: UIScheduler()
        ).startWithResult { result in
            switch result {
            case let .success(next):
                print("AddToWish Success : \(next)")
            case let .failure(error):
                // FIXME: Handle error
                print("AddToWish Error: \(error)")
            }
        }
    }
    
    func removeFromWishList(_ book: BookModel) {
        ApiHandler.removeWish(book).observe(
            on: UIScheduler()
        ).startWithResult { result in
            switch result {
            case let .success(next):
                print("RemoveFromWish Success : \(next)")
            case let .failure(error):
                // FIXME: Handle error
                print("RemoveFromWish Error: \(error)")
            }
        }
    }
    
    func addToWishList(_ book: BookModel, sender : UIBarButtonItem) {
        ApiHandler.addWish(book).observe(
            on: UIScheduler()
        ).startWithResult { result in
            switch result {
            case let .success(next):
                print("AddToWish Success : \(next)")
                UIAccessibility.post(
                    notification: UIAccessibility.Notification.layoutChanged,
                    argument: sender
                );
            case let .failure(error):
                // FIXME: Handle error
                print("AddToWish Error: \(error)")
            }
        }
    }
    
    func removeFromWishList(_ book: BookModel,sender : UIBarButtonItem) {
        ApiHandler.removeWish(book).observe(
            on: UIScheduler()
        ).startWithResult { result in
            switch result {
            case let .success(next):
                print("RemoveFromWish Success : \(next)")
                UIAccessibility.post(
                    notification: UIAccessibility.Notification.layoutChanged,
                    argument: sender
                );
            case let .failure(error):
                // FIXME: Handle error
                print("RemoveFromWish Error: \(error)")
            }
        }
    }
    
    func updateTableUI(
        result: Countable,
        prevButton: UIButton,
        nextButton: UIButton,
        pageController: UIView,
        nbPages: UILabel
    ) {
        let totalBooks = result.count
        self.availableBooks = result.books
        
        if (totalBooks > paginationParams.resultsPerPage){
            var nbPage = totalBooks/paginationParams.resultsPerPage
            if (totalBooks % paginationParams.resultsPerPage > 0) {
                nbPage += 1
            }

            view.navigationItem.title = view.navigationItem.title! + " (\(totalBooks) livres)"
            nbPages.text = "page " + String(paginationParams.page + 1) + " sur " + String(nbPage)

        }
        var books: [BookModel]
        if totalBooks == self.availableBooks.count {
            // Here we already have all books available in cache, we paginate using array
            let startingIndex = self.paginationParams.page * self.paginationParams.resultsPerPage
            let availableCount = self.availableBooks.count
            let endIndex = min(
                startingIndex + self.paginationParams.resultsPerPage,
                availableCount
            ) - 1
            if (self.availableBooks.count > 0){
                books = Array(self.availableBooks[startingIndex...endIndex])
            } else{
                books = []
                view.navigationItem.title = "Pas de " + view.navigationItem.title!
            }
        } else {
            // Not all books are available in cache, it was already paginated with a request
            books = self.availableBooks
        }
        
        log.debug("pageController = \(pageController)")
        log.debug("totalBooks = \(totalBooks)")
        log.debug("books = \(books)")
        
        self.getView()?.displayBooks(books)
        
        prevButton.isHidden = paginationParams.page == 0
        nextButton.isHidden = (paginationParams.resultsPerPage * (paginationParams.page + 1)) >= totalBooks
        pageController.isHidden = nextButton.isHidden && prevButton.isHidden
    }
    
    /*
     
     
     func updateTableUI(
     totalBooks: Int,
     prevButton: UIButton,
     nextButton: UIButton,
     pageController: UIView
     ) {
     let startingIndex = self.paginationParams.page * self.paginationParams.resultsPerPage
     let availableCount = self.availableBooks.count
     if startingIndex < availableCount {
     let endIndex = min(
     startingIndex + self.paginationParams.resultsPerPage,
     availableCount
     ) - 1
     
     let books = Array(self.availableBooks[startingIndex...endIndex])
     self.getView()?.displayBooks(books)
     
     prevButton.isHidden = self.paginationParams.page == 0
     nextButton.isHidden = (self.paginationParams.resultsPerPage * (self.paginationParams.page + 1)) >= totalBooks
     pageController.isHidden = nextButton.isHidden && prevButton.isHidden
     }
     }
     */
}
