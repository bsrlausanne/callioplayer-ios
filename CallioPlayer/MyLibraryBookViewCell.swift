//
//  MyLibraryBookViewCell.swift
//  CallioPlayer
//
//  Created by Stéphane Maillard on 25.05.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit

class MyLibraryBookViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    //@IBOutlet weak var progressBar: UIProgressView!
  
    @IBOutlet weak var toolbarButton: UIBarButtonItem!
    @IBOutlet weak var progressBar: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
