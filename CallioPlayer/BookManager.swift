//
//  BooksDataManager.swift
//  CallioPlayer
//
//  Created by Stéphane Maillard on 23.05.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import Kanna
import ReactiveCocoa
import ReactiveSwift
import AVFoundation
import RealmSwift

class BookManager {
    private static var wishlistBooks: [BookModel] = []
    private static var wishlistDirty: Bool = true
    static var libraryBooks: [BookModel] = []

    static func LoadLocalBooks() -> SignalProducer<[BookModel], NSError> {
        return SignalProducer {
            (observer: Signal<[BookModel], NSError>.Observer, _) in
            BookRepository.fetchAllBooks().observe(
                on: UIScheduler()
            ).startWithResult { result in
                switch result {
                case let .success(books):
                    libraryBooks = books
                    observer.send(value: books)
                    observer.sendCompleted()
                case let .failure(error):
                    observer.send(error: error)
                    observer.sendCompleted()
                }
            }
        }
    }
    
    static func isOnWishList(_ code:String) -> Bool {
        for book in wishlistBooks{
            if(book.code == code){
                return true
            }
        }
        return false
    }
    
    static var isWishListDirty: Bool {
        return self.wishlistDirty
    }
    
    static func setWishListDirty() {
        self.wishlistDirty = true
    }
    
    static func setWishListPristine() {
        self.wishlistDirty = false
    }
    
    static func setWishList(wishes: [BookModel]) {
        self.wishlistBooks = wishes
        self.wishlistDirty = false
    }

    static func loadWishes() {
        ApiHandler.getWishes().observe(
            on: UIScheduler()
        ).startWithResult { result in
            switch result {
            case let .success(response):
                BookManager.setWishList(wishes: response.result.wishes)
            case let .failure(error):
                log.debug("RemoveFromWish failure: \(error)")
            }
        }
    }
    
    static func saveNewBook(_ pathId: String, book: BookModel) {
        let filemanager:FileManager = FileManager()
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)[0]

        log.debug("documentsdir: \(documentsDirectory+"/"+pathId+"/"+pathId))")
        let files = filemanager.enumerator(atPath: documentsDirectory)
        let test = files?.filter{ ($0 as AnyObject).hasSuffix("\(pathId)/ncc.html")}

        if (test!.count==0){
            do{
                let fullDirPath = documentsDirectory+"/"+pathId+"/"+pathId
                let paths = try FileManager.default.contentsOfDirectory(atPath: fullDirPath)
                let contents = paths.map { aContent in (fullDirPath as NSString).appendingPathComponent(aContent)}
                
                log.debug("Searching if the book is already in memory");
                for currentFolder in contents {
                    var isDir : ObjCBool = false
                    if filemanager.fileExists(atPath: currentFolder, isDirectory:&isDir) {
                        if isDir.boolValue {
                    //log.debug("contentcurrent: \(currentFolder)")
                    let paths = try FileManager.default.contentsOfDirectory(atPath: currentFolder)
                    //log.debug("countcurrent: \(paths.count)")
                    let contents = paths.map { aContent in (currentFolder as NSString).appendingPathComponent(aContent)}
                    contents.filter{ $0.hasSuffix("/ncc.html")}.forEach({
                        file in
                        
                        log.debug("found \(file)")
                        let fullPath = file;
                        
                        let relativePath = fullPath.components(separatedBy: "Documents/")[1];
                        

                        if let parsedBook = parseBookPath(relativePath, book: book) {
                            parsedBook.display()
                            
                            BookRepository.saveBook(parsedBook).observe(
                                on: UIScheduler()
                            ).startWithResult { result in
                                switch result {
                                case let .success(success):
                                    log.debug("Success \(success)")
                                case let .failure(error):
                                    log.debug("Failure: \(error)")
                                }
                            }
                        }
                    })
                        }}

                }
            } catch{
                log.debug("erreur blab\(error)")
            }
        }else{
        
            log.debug("test: \(test?.count ?? 0)")
            log.debug("pathId: \(pathId)")
            let files = filemanager.enumerator(atPath: documentsDirectory)
            
            files?.filter{ ($0 as AnyObject).hasSuffix("\(pathId)/ncc.html") }.forEach({
                file in

                let filePath = file as! String
                log.debug("found \(documentsDirectory+"/"+filePath)")
            
                if let parsedBook = parseBookPath(filePath, book: book) {
                    parsedBook.display()
                
                    BookRepository.saveBook(parsedBook).observe(
                        on: UIScheduler()
                    ).startWithResult {result in
                        switch result {
                        case let .success(success):
                            log.debug("Success \(success)")
                        case let .failure(error):
                            log.debug("Failure: \(error)")
                        }
                    }
                }
            })
        }
    }
    
    static func removeBook(_ book: BookModel) {
        do {
            let realm = try Realm()
            let books = realm.objects(BookDB.self)
 
            for currentbook in books {
                if(book.title == currentbook.title){

                    try realm.write {
                        realm.delete(currentbook)
                    }
                    
                }
            }
        }
        catch let error {
           log.debug("An error ocurred: \(error)")
        }
    }
    
    static func bookInLibrary(_ code: String) -> Bool {
        return libraryBooks.contains { book in
            return book.code == code
        }
    }
    
    static func resetBook(_ book: BookModel) {
        do {
            let realm = try Realm()
            let books = realm.objects(BookDB.self)
            
            for currentbook in books {
                if(book.title == currentbook.title){
                    try realm.write {
                        currentbook.currentReading?.isReading=false
                    }
                }
            }
        }
        catch let error {
            log.debug("An error ocurred: \(error)")
        }
        /*BookRepository.removeBook(book).observeOn(UIScheduler()).startWithNext {
         success in
         log.debug("Success deleting\(success)")
         }*/
        
    }
    
    fileprivate static func parseBookPath(_ pathString: String, book: BookModel) -> BookModel? {
        var bookModel = book
        do {
            let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)[0]
            
            
            let path = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(pathString)
            // let path = NSURL(fileURLWithPath: pathString)

            log.debug("fullpath\(path)")
            var html : String = ""
            //var convertedString: NSString?
            do{
                html = try String(contentsOf: path, encoding: String.Encoding.utf8)
            } catch let error as NSError{
                log.debug(error.localizedDescription)
            }
            if(html == ""){
                log.debug("ascii")
                let html_temp = try String(contentsOf: path, encoding: String.Encoding.ascii)
                html = String(validatingUTF8: html_temp.cString(using: String.Encoding.utf8)!)!
            }
            log.debug(html)
            
            bookModel.id = UUID().uuidString
            log.debug(pathString)
            
            bookModel.path = String(
                pathString[..<pathString.index(pathString.endIndex, offsetBy: -8)]
            )
            
            log.debug("book path is \(bookModel.path)")

            let document = try? Kanna.HTML(html: html, encoding: String.Encoding.utf8)
            if let doc = document {
                log.debug(doc.title ?? "No title defined")
                
                if let title = doc.xpath("//meta[@name='dc:title']/@content").first?.text {
                    bookModel.title = title
                }
                
                if let publisher = doc.xpath("//meta[@name='dc:publisher']/@content").first?.text, (publisher == "BSR" || publisher.lowercased().contains("sonore romande")) {
                    bookModel.isDownloadFromBSR = true
                }
                
                if let author = doc.xpath("//meta[@name='dc:creator']/@content").first?.text {
                    bookModel.author = author
                }
                
                if let reader = doc.xpath("//meta[@name='ncc:narrator']/@content").first?.text {
                    bookModel.reader = reader
                }
                
                if let editor = doc.xpath("//meta[@name='ncc:sourceEdition']/@content").first?.text {
                    bookModel.editor = editor
                }
                
                if let media = doc.xpath("//meta[@name='ncc:totalTime']/@content").first?.text {
                    bookModel.media = media
                }
                
                let chapters = doc.xpath("//h1 | //h2 | //h3 | //h4 | //h5 | //h6")
                
                log.debug(chapters.count)
                
                var totalDuration = 0
                
                for chapter in chapters {
                    if var bookChapter = retrieveChapter(
                        chapter,
                        bookPath: path.deletingLastPathComponent(),
                        bookPathShortString: bookModel.path
                    ) {
                        bookChapter.startAt = totalDuration
                        bookModel.chapters.append(bookChapter)
                        
                        totalDuration += bookChapter.duration
                    }
                }
                
                bookModel.duration = totalDuration
            }
            
            return bookModel

        } catch let error as NSError {
            log.debug(error.localizedDescription)
        }
        
        return nil
    }
    
    fileprivate static func retrieveChapter(_ chapter: XMLElement, bookPath: URL, bookPathShortString: String) -> BookChapter? {
        log.debug("bookPath = \(bookPath), bookPathShortString = \(bookPathShortString)")
        do {
            var bookChapter = BookChapter()
            
            bookChapter.chapterId = chapter["id"] ?? ""
            bookChapter.title = chapter.xpath("a").first?.text ?? ""
            
            switch chapter.tagName ?? "" {
            case "h1":
                bookChapter.level = 1
                break
            case "h2":
                bookChapter.level = 2
                break
            case "h3":
                bookChapter.level = 3
                break
            case "h4":
                bookChapter.level = 4
                break
            case "h5":
                bookChapter.level = 5
                break
            case "h6":
                bookChapter.level = 6
                break
            default:
                bookChapter.level = 0
            }
            
            if let smilName = chapter.xpath("a/@href").first?.text?.components(separatedBy: "#")[0] {
                let smilPath = bookPath.appendingPathComponent(smilName)
                let xml = try String(contentsOf: smilPath, encoding: String.Encoding.windowsCP1252)
                
                let document = try? Kanna.XML(xml: xml, encoding: String.Encoding.windowsCP1252)
                if let doc = document {
                    if let src = doc.xpath("//audio/@src").first?.text {
                        bookChapter.audioFile = bookPathShortString + src
                        log.debug("bookChapter audioFile = \(bookChapter.audioFile)")
                        
                        bookChapter.duration = Int(CMTimeGetSeconds(AVAsset.init(url: bookPath.appendingPathComponent(src)).duration))
                    }
                }
            } else {
                return nil
            }
            
            return bookChapter
        }
        catch let error as NSError{
            log.debug(error.localizedDescription)
        }
        
        return nil
    }
}















