//
//  AppDelegate.swift
//  CallioPlayer
//
//  Created by Stéphane Maillard on 20.05.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit
import XCGLogger
import AVFoundation
import RealmSwift
import UserNotifications

let appDelegate = UIApplication.shared.delegate as! AppDelegate

let log: XCGLogger = INIT_XCGLOGGER()

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    
    let documentsxDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
    
    let cacheDirectory: URL = {
        let urls = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        CatalogNewBooksViewController.load()
        CatalogWishListViewController.load()
        CatalogSearchViewController.load()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            if granted {
                print("Local notification authorization granted")
            } else {
                print("Local notification authorization denied")
            }
        }
        UNUserNotificationCenter.current().delegate = self
        return true
    }
    

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        log.debug("terminating hello from delegate")
        //BookReaderPresenter.saveBookState()
    }
    
    // Implement the new UNUserNotificationCenterDelegate method for handling foreground notifications
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let alertTitle = notification.request.content.title
//        let alertBody = notification.request.content.body
//
//        let alert = UIAlertController(title: alertTitle, message: alertBody, preferredStyle: .actionSheet)
//        alert.modalPresentationStyle = .popover
//
//        if let presenter = alert.popoverPresentationController {
//            presenter.sourceRect = CGRect(x: 0, y: 0, width: 1.0, height: 1.0)
//            presenter.sourceView = self.window?.rootViewController?.view
//        }
//
//        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//
//        let delay = 3.0 * Double(NSEC_PER_SEC)
//        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
//        DispatchQueue.main.asyncAfter(deadline: time) {
//            alert.dismiss(animated: true, completion: nil)
//        }

//         Call the completion handler to display the notification when the app is in the foreground
        completionHandler([.alert, .sound])
    }
//
//    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
//
//        let alert  = UIAlertController(title:notification.alertTitle, message:notification.alertBody, preferredStyle: .actionSheet)
//        alert.modalPresentationStyle = .popover
//
//        if let presenter = alert.popoverPresentationController {
//
//            presenter.sourceRect = CGRect(x: 0,y: 0,width: 1.0,height: 1.0);
//            presenter.sourceView = self.window?.rootViewController?.view
//        }
//
//        self.window?.rootViewController?.present(alert, animated:true, completion:nil)
//
//        let delay = 3.0*Double(NSEC_PER_SEC)
//        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
//        DispatchQueue.main.asyncAfter(deadline: time, execute: {
//            alert.dismiss(animated: true, completion:nil)
//        })
//    }
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        do {
            let session = AVAudioSession.sharedInstance()
            // let category = convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)
            if #available(iOS 10.0, *) {
                try session.setCategory(
                    AVAudioSession.Category.playback,
                    mode: .default,
                    options: []
                )
            } else {
                session.perform(
                    NSSelectorFromString("setCategory:error:"),
                    with: AVAudioSession.Category.playback)
            }

            self.becomeFirstResponder()
            UIApplication.shared.beginReceivingRemoteControlEvents()
            
            // prevent sleep mode
            UIApplication.shared.isIdleTimerDisabled = true
            
            // DatabaseUtils.getDefaultRealmInstance()
//            application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert], categories: nil))
            
            let schemaVersion: UInt64 = 3 // Increment this each time your schema changes
            let config = Realm.Configuration(
                schemaVersion: schemaVersion,
                migrationBlock: { migration, oldSchemaVersion in
    
                    if (oldSchemaVersion < schemaVersion) {
                        // See https://realm.io/docs/swift/latest/#migrations
                    }
            })
            
            Realm.Configuration.defaultConfiguration = config
            log.debug(Realm.Configuration.defaultConfiguration.fileURL)
            
            let realm = try! Realm()
            let books = realm.objects(BookDB.self)
            
            // select which tab to show at startup. by default login/catalog tab.
            var startingTabIndex = 2
            
            if(books.count > 0) {
                startingTabIndex = 1
                let currentBooks = books.filter{
                    book in
                    return book.currentReading!.isReading
                }
                if(currentBooks.count > 0){
                    startingTabIndex = 0
                }
            }
            
            var customFont = UIFont.systemFont(ofSize: 13)
            var navTitleFont = UIFont.systemFont(ofSize: 21)
            
            let rvController = self.window!.rootViewController
            if (rvController!.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular &&
                rvController!.view.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular) {
                 customFont = UIFont.systemFont(ofSize: 26)
                 navTitleFont = UIFont.systemFont(ofSize: 42)
            } else {
                 customFont = UIFont.systemFont(ofSize: 13)
                 navTitleFont = UIFont.systemFont(ofSize: 21)
            }
            
            UINavigationBar.appearance().titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([ NSAttributedString.Key.font.rawValue: navTitleFont, NSAttributedString.Key.foregroundColor.rawValue : UIColor.white])
            
            UINavigationBar.appearance().setBackgroundImage(UIImage.imageWithColor(UIColor.bsrBlue), for: .default)

            UINavigationBar.appearance().shadowImage = UIImage.imageWithColor(UIColor.white)
            
            UIBarButtonItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): customFont]), for: UIControl.State())
            
            UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): customFont]), for: UIControl.State())
            
            UITabBar.appearance().backgroundImage = UIImage.imageWithColor(UIColor.white)
            UITabBar.appearance().shadowImage = UIImage.imageWithColor(UIColor.bsrBlue)
            
            UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): UIColor.red]), for:UIControl.State())

            // For each tab item..
            
            
            
            //log.debug("livres trouvés au démarrage: \(books.first)")
            if self.window!.rootViewController as? UITabBarController != nil {
                let tababarController = self.window!.rootViewController as! UITabBarController
                tababarController.selectedIndex = startingTabIndex            }
        } catch {
            log.debug("\(error)")
        }
        
        return true
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
