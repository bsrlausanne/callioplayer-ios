//
//  BookData.swift
//  CallioPlayer
//
//  Created by Stéphane Maillard on 23.05.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct BookModel: Decodable {
    var id: String
    var title: String
    var summary: String?
    var author: String
    var cover: String?
    var editor: String?
    var duration: Int?
    var reader: String?
    var media: String?
    var category: String?
    var producer: String?
    var producerCode: String?
    var isbn: String?
    var downloadUri: String?
    var sampleMp3Uri: String?
    var code: String
    var position: Int = 0

    var currentRate: Float = 1.0
    var timeSeek: Double = 30
    var isDownloadFromBSR = false
    var path: String = ""
    
    // reader
    var chapters: [BookChapter] = []
    var currentChapter = BookChapter()
    var currentPosition = 0
    var bookmarks:[BookBookmark] = []
    var downloadDate = Date()
    var lastPlayDate = Date()

    var currentReading = BookCurrentReading()
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case summary
        case author
        case cover
        case editor
        case duration
        case year
        case reader
        case media
        case category
        case producer
        case producerCode
        case isbn
        case downloadUri
        case files
        case code
        case position
    }
    
    enum FileKeys: String, CodingKey {
        case samples
        case zip
        case cacheable_uri
    }
    
    func display() {}
    
    init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let filesContainer = try container.nestedContainer(keyedBy: FileKeys.self, forKey: .files)
        
        self.id = try container.decode(String.self, forKey: .id)

        let year = (try container.decodeIfPresent(String.self, forKey: .year)) ?? ""
        let editor = (try container.decodeIfPresent(String.self, forKey: .editor)) ?? ""
        self.editor = "\(editor), \(year)"

        self.reader = try container.decodeIfPresent(String.self, forKey: .reader)
        self.isbn = try container.decodeIfPresent(String.self, forKey: .isbn)
        self.author = try container.decode(String.self, forKey: .author)
        self.cover = try container.decodeIfPresent(String.self, forKey: .cover)
        self.duration = try container.decodeIfPresent(Int.self, forKey: .duration)
        self.producerCode = try container.decodeIfPresent(String.self, forKey: .producerCode)
        self.producer = try container.decodeIfPresent(String.self, forKey: .producer)
        self.media = try container.decodeIfPresent(String.self, forKey: .media)
        self.title = try container.decode(String.self, forKey: .title)
        self.summary = try container.decodeIfPresent(String.self, forKey: .summary)
        self.category = try container.decodeIfPresent(String.self, forKey: .category)
        
        let samples = try filesContainer.decodeIfPresent([String].self, forKey: .samples)
        let zip = try filesContainer.decodeIfPresent(BookZip.self, forKey: .zip)
        self.sampleMp3Uri = samples?.last
        self.downloadUri = zip?.uri
        
        self.code = try container.decode(String.self, forKey: .code)
        self.position = try container.decodeIfPresent(Int.self, forKey: .position) ?? 0
    }
    
    init(bookDB: BookDB) {
        self.id = bookDB.id
        self.editor = bookDB.editor
        self.reader = bookDB.reader
        self.author = bookDB.author
        self.duration = bookDB.duration
        self.media = bookDB.media
        self.title = bookDB.title
        self.path = bookDB.path
        self.code = bookDB.code
        self.currentRate = bookDB.currentRate
        self.timeSeek = bookDB.timeSeek
        self.cover = bookDB.cover

        self.currentReading = BookCurrentReading(currentReadingDB: bookDB.currentReading!)
        self.bookmarks = bookDB.bookmarks.map {
            bookmarkDB in
            return BookBookmark(bookmarkDB: bookmarkDB)
        }
        
        self.chapters = bookDB.chapters.map {
            chapterDB in
            return BookChapter(chapterDB: chapterDB)
        }
    }
    
    func toDB() -> BookDB {
        let bookDB = BookDB()
        
        bookDB.id = self.id
        bookDB.code = self.code
        bookDB.editor = self.editor ?? ""
        bookDB.reader = self.reader ?? ""
        bookDB.author = self.author
        bookDB.cover = self.cover ?? ""
        bookDB.duration = self.duration ?? 0
        bookDB.media = self.media ?? ""
        bookDB.title = self.title
        bookDB.path = self.path
        bookDB.currentRate = self.currentRate
        bookDB.timeSeek = self.timeSeek
        bookDB.currentReading = self.currentReading.toDB()
        
        self.bookmarks.forEach {
            bookmark in
            bookDB.bookmarks.append(bookmark.toDB())
        }
        
        self.chapters.forEach {
            chapter in
            bookDB.chapters.append(chapter.toDB())
        }
        
        return bookDB
    }
}

struct BookZip: Decodable {
    var uri: String
    var size: Int
}
