//
//  DatabaseUtils.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 31.05.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseUtils {
    static let sharedConfiguration: Realm.Configuration = Realm.Configuration(
        inMemoryIdentifier: "Callio_Player_Database_Realm"
    )
    
    private static var defaultRealmInstance: Realm?
    
    static func getDefaultRealmInstance() -> Realm {
        if defaultRealmInstance == nil {
            defaultRealmInstance = try! Realm(configuration: sharedConfiguration)
        }
        
        return defaultRealmInstance!
    }
}
