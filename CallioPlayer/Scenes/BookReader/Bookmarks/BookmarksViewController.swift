//
//  BookmarksViewController.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 11.08.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import AVFoundation
import UIKit

class BookmarksViewController: UITableViewController {
    
    var bookmarks: [BookBookmark] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    @objc func goToBookmark(_ sender: UITapGestureRecognizer) {
        if let button = sender.view as? UILabel {
            let selectedBookmark = bookmarks[button.tag]
            
            print("chapter \(selectedBookmark.title) selected")
            
            let parentController = self.parent as! BookReaderViewController
            parentController.setBookmark(selectedBookmark)
        }
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookmarks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "BookmarksViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BookmarksViewCell
        cell.accessibilityElements = [cell.timeLabel, cell.titleLabel, cell.removeBookmarkButton];
        let bookmark = bookmarks[indexPath.row]
        
        cell.titleLabel.text = bookmark.title
        cell.timeLabel.text = CMTimeMakeWithSeconds(Double(bookmark.duration), preferredTimescale: 1).durationText
        cell.timeLabel.accessibilityLabel = parseTimeStringAccessibility(bookmark.duration)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(goToBookmark))
        cell.titleLabel.isUserInteractionEnabled = true
        cell.titleLabel.tag = indexPath.row
        cell.titleLabel.addGestureRecognizer(tap)
        cell.removeBookmarkButton.addTarget(self, action: #selector(removeBookmark), for: .touchUpInside)
        cell.removeBookmarkButton.tag = indexPath.row
        cell.removeBookmarkButton.accessibilityLabel = "Supprimer le signet"
        
        return cell
    }
    
    // duration : position en secondes
    func parseTimeStringAccessibility(_ duration: Int) -> String {
        
        var toReturn : String
        
        if(duration < 60) {
            toReturn = "Position: " + String(duration) + "secondes";
        }
        else if (duration < 3600) {
            let minutes = duration / 60
            let secondes = duration - minutes * 60
            toReturn = "Position: " + String(minutes) + "minutes" + String(secondes) + "secondes" ;
        }
        else {
            let hours = duration / 3600;
            let minutes = (duration - hours * 3600 ) / 60;
            toReturn = "Position: " + String(hours) + "h." + String(minutes) + "minutes";
        }
        return toReturn;
    }
    
    @objc func removeBookmark(_ sender: UIButton) {
        
        
        let alertController = UIAlertController(title: "Supprimer le signet", message: bookmarks[sender.tag].title + " - " + CMTimeMakeWithSeconds(Double(bookmarks[sender.tag].duration), preferredTimescale: 1).durationText,  preferredStyle: .alert)
        
        let oui = UIAlertAction(title: "Oui", style: .default, handler: { (UIAlertAction) -> Void in
            let row = sender.tag
            self.bookmarks.remove(at: row)
            
            // Sauvegarde de la suppression du signet dans le livre
            
            let parentController = self.parent as! BookReaderViewController
            parentController.removeBookmark(row)
            self.updateView()
            
        })
        let non = UIAlertAction(title: "Non", style: .default, handler: nil)
        alertController.addAction(oui)
        alertController.addAction(non)
        
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: Event handling
    
    func updateView() {
        self.tableView.reloadData()
    }
    
    // MARK: Display logic
    
}
