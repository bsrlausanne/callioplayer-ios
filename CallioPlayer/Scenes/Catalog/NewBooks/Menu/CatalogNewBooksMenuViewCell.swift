//
//  CatalogNewBooksMenuViewCell.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 19.07.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit

class CatalogNewBooksMenuViewCell: UITableViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
