//
//  File.swift
//  CallioPlayer
//
//  Created by Faton Ramadani on 28.02.17.
//  Copyright © 2017 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

class Configurator {
    static let sharedInstance = Configurator()
    
    func configure<T: KeyDecodable & Countable>(_ viewController: Controller<T>) {
        let presenter = Presenter<T>()
        presenter.setView(viewController)
        
        viewController.presenter = presenter
    }
}
