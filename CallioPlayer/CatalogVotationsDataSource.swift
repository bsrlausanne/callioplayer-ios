//
//  CatalogVotationsDataSource.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 19.04.23.
//  Copyright © 2023 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit
import ReactiveSwift

class CatalogVotationsDataSource: TableViewDataSource {
    
    typealias T = GetVotationsResponse
    
    var navigationTitle: String = "Votations"
    
    var downloadBooksCallback: (PaginationParameters) -> SignalProducer<WebServiceResponse<GetVotationsResponse>, NSError> {
        return { _ in
            return ApiHandler.getVotations()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, viewController: Controller<GetVotationsResponse>) -> UITableViewCell {
        let options = BuildTableViewOptions(
            books: viewController.books,
            cellIdentifier: "CatalogVotationsViewCell",
            readSampleCallback: viewController.readSample,
            downloadBookCallback: viewController.downloadPressed,
            bookmarkBookCallback: viewController.bookmarkPressed
        )
        
        return buildTableView(tableView, cellForRowAt: indexPath, options: options, CatalogVotationsViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int, viewController: Controller<GetVotationsResponse>) -> Int {
        return viewController.books.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, viewController: Controller<GetVotationsResponse>) {
        DispatchQueue.main.async { () -> Void in
            viewController.performSegue(withIdentifier: "ShowBookFromList", sender: tableView.cellForRow(at: indexPath))
        }
    }
}
