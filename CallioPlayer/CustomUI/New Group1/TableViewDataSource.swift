//
//  CommonDataSource.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 02.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift

protocol TableViewDataSource {
    associatedtype T: KeyDecodable & Countable
    
    var navigationTitle: String { get }
    var downloadBooksCallback: (PaginationParameters) -> SignalProducer<WebServiceResponse<T>, NSError> { get }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, viewController: Controller<T>) -> UITableViewCell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int, viewController: Controller<T>) -> Int
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, viewController: Controller<T>)
}

extension TableViewDataSource {
    func buildTableView<T: BookListViewCell>(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath,
        options: BuildTableViewOptions,
        _ viewCellType: T.Type
    ) -> UITableViewCell {
        let books = options.books
        let cellIdentifier = options.cellIdentifier
        log.debug("cell self is = \(self)")
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! T
        
        if books.count > 0 {
            // self.navigationItem.title = navigationTitle ?? "Résultats, page \(options.currentPageNb)"
            let book = books[indexPath.row]
            return cell.setupLayout(
                book: book,
                tag: indexPath.row,
                downloadCb: options.downloadBookCallback,
                bookmarkCb: options.bookmarkBookCallback,
                readSampleCb: options.readSampleCallback,
                cellIdentifier: cellIdentifier
            )
        } else {
            cell.authorLabel.text = ""
            cell.titleLabel.text = "Aucun livre trouvé."
            cell.durationLabel.text = "Elargissez votre recherche."
            cell.isUserInteractionEnabled = false
            cell.toolbar.isHidden = true
            
            return cell
            
        }
    }
}
