//
//  BookListViewCell.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 02.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

class BookListViewCell: UITableViewCell {
    // MARK: Properties
    private var _titleLabel: UILabel?
    private var thumbnailWidth: CGFloat = 80
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    var thumbnailImage: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private var downloadCb: ((UIBarButtonItem) -> Void)?
    private var bookmarkCb: ((UIBarButtonItem) -> Void)?
    private var readSampleCb: ((UIBarButtonItem) -> Void)?
    
    @IBOutlet weak var downloadButton: UIBarButtonItem!
    @IBOutlet weak var bookmarkButton: UIBarButtonItem!
    @IBOutlet weak var playButton: UIBarButtonItem!
    
    @IBOutlet weak var toolbar: UIToolbar!
    
    private var isTextOnlyModeEnabled: Bool {
        let ret = UserDefaults.standard.bool(forKey: SettingsMenu.textOnlyModeKey)
        log.debug("textOnlyModeEnabled = \(ret)")
        return ret
    }
    
    private var bookDetailsViews: [UIView] {
        return [
            authorLabel,
            titleLabel,
            durationLabel,
            (!isTextOnlyModeEnabled) ? thumbnailImage : nil
        ].filter{$0 != nil}.map{$0!}
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        refreshLayout()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func refreshLayout() {
        toolbar.accessibilityElementsHidden = true

        bookDetailsViews.forEach { view in
            view.removeFromSuperview()
            self.addSubview(view)
        }
        setupConstraints()
    }
    
    func setupLayout(
        book: BookModel,
        tag: Int,
        downloadCb: @escaping (UIBarButtonItem) -> Void,
        bookmarkCb: @escaping (UIBarButtonItem) -> Void,
        readSampleCb: @escaping (UIBarButtonItem) -> Void,
        cellIdentifier: String
    ) -> Self {
        // Store callbacks
        self.downloadCb = downloadCb
        self.bookmarkCb = bookmarkCb
        self.readSampleCb = readSampleCb
        
        self.titleLabel.numberOfLines = 1
        self.titleLabel.lineBreakMode = .byTruncatingTail

        self.authorLabel.text = book.author
        self.titleLabel.text = book.title
        

        if isTextOnlyModeEnabled {
            self.thumbnailImage.image = nil
        } else {
            self.thumbnailImage.image = UIImage(named: "loading")?.cropped(to: CGSize(width: self.thumbnailWidth,height: self.frame.height))?.imageWithInsets(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
            
            self.thumbnailImage.assignCoverAsync(
                book: book,
                width: self.thumbnailWidth,
                height: self.frame.height,
                insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            )
        }
        
        if let m = book.media {
            self.durationLabel.text = m.replace("00:", withString: "").replace(":", withString: "h. ") + "min."
        } else {
            self.durationLabel.text = "-"
        }
        
        if cellIdentifier != "CatalogFreeBooksViewCell" {
            ApiHandler.checkIfOnWishList(book.code) { success in
                if success {
                    self.bookmarkButton.image = UIImage(named: "ic_star_white")
                    self.bookmarkButton.accessibilityLabel = "Retirer de la liste de voeux."
                } else {
                    self.bookmarkButton.image = UIImage(named: "ic_star_border_white")
                    self.bookmarkButton.accessibilityLabel = "Ajouter à la liste de voeux."
                }
            }
        }
        var alreadyThere = false
        var accessibleText = "télécharger \(book.title)"
        
        log.debug("book \(book.code) is in library ? \(BookManager.bookInLibrary(book.code))")
        if BookManager.bookInLibrary(book.code) {
            alreadyThere = true
            accessibleText = "livre déjà sur l'appareil"
        }
        if DownloadUtils.isBookDownloading(book.code) {
            alreadyThere = true
            accessibleText = "en cours de téléchargement"
        }
        self.downloadButton.isEnabled = !alreadyThere
        
        self.downloadButton.target = self
        self.downloadButton.action = #selector(onDownload)
        self.downloadButton.tag = tag
        self.downloadButton.accessibilityLabel = accessibleText
        
        self.playButton.target = self
        self.playButton.action = #selector(onReadSample)
        self.playButton.tag = tag
        self.playButton.accessibilityLabel = "écouter un extrait de \(book.title)"
        
        self.bookmarkButton.target = self
        self.bookmarkButton.action = #selector(onBookmark)
        self.bookmarkButton?.tag = tag
        

        if cellIdentifier == "CatalogFreeBooksViewCell" {
            debugPrint("testhugo")
            self.bookmarkButton.isEnabled = false
            self.bookmarkButton.tintColor = UIColor.clear
        }
        // harcodé : cacher boutton liste de voeux pour les livres de test
        // todo : cacher en fonction d'un autre paramètre
//        if book.code == "20963" || book.code == "19932" || book.code == "17124" {
//            self.bookmarkButton.isEnabled = false
//            self.bookmarkButton.tintColor = UIColor.clear
//        }
        
        return self
    }
    
    func setupConstraints() {
        // Enable AutoLayout
        bookDetailsViews.forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
            view.removeAllConstraints()
        }
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        toolbar.removeConstraints(toolbar.constraints)
        
        let margins = self.layoutMarginsGuide
        let textLeftAnchor = (isTextOnlyModeEnabled) ? margins.leftAnchor : thumbnailImage.rightAnchor
        let toolbarLeftAnchor = (isTextOnlyModeEnabled) ? self.leftAnchor : thumbnailImage.rightAnchor

        var constraints: [NSLayoutConstraint] = [
            // Widths
            authorLabel.leftAnchor.constraint(equalTo: textLeftAnchor),
            authorLabel.rightAnchor.constraint(equalTo: margins.rightAnchor),
            titleLabel.leftAnchor.constraint(equalTo: textLeftAnchor),
            titleLabel.rightAnchor.constraint(equalTo: margins.rightAnchor),
            durationLabel.leftAnchor.constraint(equalTo: textLeftAnchor),
            durationLabel.rightAnchor.constraint(equalTo: margins.rightAnchor),
            toolbar.leftAnchor.constraint(equalTo: toolbarLeftAnchor),
            toolbar.rightAnchor.constraint(equalTo: self.rightAnchor),
            
            // Vertical placement
            authorLabel.topAnchor.constraint(equalTo: margins.topAnchor),
            titleLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor),
            durationLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            toolbar.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            
            // Heights
            // self.heightAnchor
        ]
                
        if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular &&
            self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular) {
            thumbnailWidth = 130
        } else{
            thumbnailWidth = 80
        }
        
        let thumbnailConstraints: [NSLayoutConstraint] = [
            thumbnailImage.leftAnchor.constraint(equalTo: self.leftAnchor),
            thumbnailImage.topAnchor.constraint(equalTo: self.topAnchor),
            thumbnailImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            thumbnailImage.widthAnchor.constraint(equalToConstant: self.thumbnailWidth),
            //thumbnailImage.heightAnchor.constraint(equalTo: thumbnailImage.widthAnchor, multiplier: 1.45),
            //thumbnailImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            //thumbnailImage.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ]
        
        if !isTextOnlyModeEnabled {
            constraints.append(contentsOf: thumbnailConstraints)
        }
        
        
        
        var views = bookDetailsViews
        views.append(toolbar)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func onDownload(_ sender: UIBarButtonItem) {
        downloadCb?(sender)
    }

    @objc private func onBookmark(_ sender: UIBarButtonItem)  {
        bookmarkCb?(sender)
    }

    @objc private func onReadSample(_ sender: UIBarButtonItem) {
        log.debug("READ SAMPLE CALLBACK")
        readSampleCb?(sender)
    }
}
