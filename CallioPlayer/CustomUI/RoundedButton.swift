//
//  RoundedButton.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 03.12.20.
//  Copyright © 2020 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

//https://www.youtube.com/watch?v=zatzD2z5IMg
class RoundedBlueBorderButton: UIButton {
    override func awakeFromNib(){
        super.awakeFromNib()
        
        layer.borderWidth = 2/UIScreen.main.nativeScale
        layer.borderColor = UIColor.bsrBlue.cgColor
        contentEdgeInsets = UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height/3
        
    }
}

class RoundedWhiteBorderButton: UIButton {
    override func awakeFromNib(){
        super.awakeFromNib()
        
        layer.borderWidth = 2/UIScreen.main.nativeScale
        layer.borderColor = UIColor.white.cgColor
        contentEdgeInsets = UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height/3
        
    }
}
