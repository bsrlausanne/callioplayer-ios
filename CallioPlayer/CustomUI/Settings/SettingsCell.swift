//
//  SettingsCell.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 27.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit


class SettingsCell: BaseCell {
    var setting: Setting? {
        didSet {
            self.label.numberOfLines = 0
            self.label.lineBreakMode = .byWordWrapping
            self.label.text = setting?.label
            if let imageName = setting?.imageName {
                let image = UIImage(
                    named: imageName
                )?.changeColor(UIColor.bsrBlue)
                
                self.iconImageView.image = image
            }

            if let controlElement = setting?.controlElement {
                self.controlUi = controlElement
                self.pressedCallback = setting?.pressedCallback
            } else if let callback = setting?.pressedCallback {
                self.pressedCallback = callback
                self.controlUi?.removeFromSuperview()
                self.controlUi = nil
            }
            
            self.removeAllConstraints()
            self.setupConstraints()
        }
    }
    
    private var label: UILabel = {
        let l = UILabel()
        
        return l
    }()
    private var iconImageView: UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        return v
    }()
    private var controlUi: UISwitch?
    private var pressedCallback: (() -> Void)?
    
    override func setupViews() {
        super.setupViews()
        
        // Required to issue non-conflicting constraints upon setup
        self.translatesAutoresizingMaskIntoConstraints = false
        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.iconImageView.translatesAutoresizingMaskIntoConstraints = false
        self.controlUi?.translatesAutoresizingMaskIntoConstraints = false

        self.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(handleTap))
        )
    }
    

    private func setupConstraints() {
        var format: String = "H:|-8-[v0(30)]-8-[v1]-68-|"
        
        self.contentView.addSubview(self.iconImageView)
        self.contentView.addSubview(self.label)
        
        if let controlUi = self.controlUi {
            format = "H:|-8-[v0(30)]-8-[v1]-8-[v2(60)]|"
            self.contentView.addSubview(controlUi)
        }
        
        let views = [self.iconImageView, self.label, self.controlUi].filter({$0 != nil}).map({$0!})
        self.addConstraintsWithFormat(
            format: format,
            views: views
        )
        
        self.iconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.controlUi?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    @objc private func handleTap() {
        if let uiSwitch = self.controlUi {
            if let _ = self.pressedCallback {
                uiSwitch.setOn(!uiSwitch.isOn, animated: true)
            }
        }
        
        self.pressedCallback?()
    }
}


class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupViews() {}
}
