//
//  Settings.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 26.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

class SettingsMenu: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    static let textOnlyModeKey = "TEXT_ONLY_MODE_ENABLED"
    static let inReadingTabKey = "IN_READING_TAB_ENABLED"
    
    private let blackView = UIView()
    private let menuView = UICollectionView(
        frame: .zero,
        collectionViewLayout: UICollectionViewFlowLayout()
    )

    private let inReadingTabSwitch: UISwitch = {
        let s = UISwitch()
        let isOn = UserDefaults.standard.bool(forKey: inReadingTabKey)
        s.setOn(isOn, animated: false)
        return s
    }()
    private let textOnlySwitch: UISwitch = {
        let s = UISwitch()
        let isOn = UserDefaults.standard.bool(forKey: textOnlyModeKey)
        s.setOn(isOn, animated: false)
        return s
    }()

    private var settings: [Setting] = []
    private let cellId = "settingCellId"
    private let cellHeight: CGFloat = 50
    private let cellSpacing: CGFloat = 10
    
    override init() {
        super.init()

        self.menuView.dataSource = self
        self.menuView.delegate = self
        self.menuView.register(
            SettingsCell.self,
            forCellWithReuseIdentifier: cellId
        )
        
        inReadingTabSwitch.addTarget(self, action: #selector(handleInReadingToggle), for: .valueChanged)
        textOnlySwitch.addTarget(self, action: #selector(handleTextOnlyToggle), for: .valueChanged)
        
        self.settings = [
            Setting(
                label: "Activer l'onglet \"Lecture en cours\"",
                imageName: "ic_settings_white",
                controlElement: inReadingTabSwitch,
                pressedCallback: handleInReadingToggle
            ),
            Setting(
                label: "Affichage texte uniquement",
                imageName: "",
                controlElement: textOnlySwitch
            ),
            Setting(
                label: "Fermer les paramètres",
                imageName: "clear_btn",
                pressedCallback: self.handleDismiss
            )
        ]
    }
    
    func attachToNavigationItem(_ navItem: UINavigationItem) {
        let settingsImage = UIImage(
            named: "ic_settings_white"
        )?.changeColor(UIColor.init(white: 1, alpha: 0.9))
        
        let settingsButtonItem = UIBarButtonItem(
            image: settingsImage,
            style: .plain,
            target: self,
            action: #selector(showSettings)
        )
        settingsButtonItem.accessibilityLabel = NSLocalizedString("Boutons.Settings", comment: "")
        
        if navItem.rightBarButtonItems == nil {
            navItem.rightBarButtonItems = []
        }
        navItem.rightBarButtonItems?.append(settingsButtonItem)
    }
    
    @objc func showSettings() {
        if let window = UIApplication.shared.keyWindow {
            self.blackView.alpha = 0
            self.blackView.frame = window.frame
            self.blackView.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
            self.blackView.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(handleDismiss)
                )
            )
            
            let height = (self.cellHeight + self.cellSpacing) * CGFloat(self.settings.count)
            let y = window.frame.height - height
            self.menuView.backgroundColor = UIColor.white
            self.menuView.frame = CGRect(
                x: 0,
                y: window.frame.height,
                width: window.frame.width,
                height: height
            )
            
            window.addSubview(self.blackView)
            window.addSubview(self.menuView)
            
            UIView.animate(
                withDuration: 0.5,
                delay: 0,
                usingSpringWithDamping: 1,
                initialSpringVelocity: 1,
                options: .curveEaseOut,
                animations: {
                    self.blackView.alpha = 1
                    self.menuView.frame = CGRect(
                        x: 0,
                        y: y,
                        width: self.menuView.frame.width,
                        height: self.menuView.frame.height
                    )
                }
            )
        }
    }
    
    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow {
                self.menuView.frame = CGRect(
                    x: 0,
                    y: window.frame.height,
                    width: self.menuView.frame.width,
                    height: self.menuView.frame.height
                )
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.menuView.dequeueReusableCell(
            withReuseIdentifier: cellId,
            for: indexPath
        ) as! SettingsCell
        
        log.debug("setting \(indexPath.item)")
        cell.setting = self.settings[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(
            width: self.menuView.frame.width,
            height: self.cellHeight
        )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.cellSpacing
    }
    
    @objc private func handleInReadingToggle() {
         let userDefaults = UserDefaults.standard
        userDefaults.set(self.inReadingTabSwitch.isOn, forKey: SettingsMenu.inReadingTabKey)
        let res = userDefaults.synchronize()
        log.debug("Result of saving \(SettingsMenu.inReadingTabKey) is \(res)")
        
        TabBarHelper.refreshTabItemsList()
    }
    
    @objc private func handleTextOnlyToggle() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(self.textOnlySwitch.isOn, forKey: SettingsMenu.textOnlyModeKey)
        let res = userDefaults.synchronize()
        log.debug("Result of saving \(SettingsMenu.textOnlyModeKey) is \(res)")
    }
}
