//
//  CallioTabBar.swift
//  CallioPlayer
//
//  Created by Simon BSR on 01.03.17.
//  Copyright © 2017 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

class CallioTabBar: UITabBar {
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var size = super.sizeThatFits(size)
        
        
        size.height = 72
        return size
    }
    
    
    func viewDidLoad() {
        self.tintColor = UIColor.white
    }
}
