//
//  TabBarLauncher.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 28.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

class TabBarHelper {
    
    private static let imageNames = [
        "ic_bookmark_white_36pt",
        "ic_library_books_36pt",
        "ic_search_36pt"
    ]
    
    private static let accessibilityLabels = [
        "Lecture en cours",
        "Bibliothèque",
        "Catalogue"
    ]

    private static let tabBarController: UITabBarController? = {
        let app = UIApplication.shared
        return app.keyWindow?.rootViewController as? UITabBarController
    }()
    
    private static let tabBar: UITabBar? = tabBarController?.tabBar
    
    private static var initialViewControllers: [UIViewController]?
    
    private static var filteredViewControllers: [UIViewController] {
        return tabBarController?.viewControllers?.filter { viewController in
            let nc = viewController as? UINavigationController
            return !(nc?.viewControllers.first is CurrentBooksViewController)
        } ?? []
    }

    private static var isInReadingTabEnabled: Bool {
        return UserDefaults.standard.bool(
            forKey: SettingsMenu.inReadingTabKey
        )
    }
    
    
    static func setupTabBar() {
        storeInitialTabs()
        refreshTabItemsList()
    }
    
    static func refreshTabItemsList() {
        log.debug(tabBarController)
        if !isInReadingTabEnabled {
            log.debug("filtered")
            log.debug(filteredViewControllers)
            tabBarController?.setViewControllers(filteredViewControllers, animated: true)
        } else {
            log.debug("initial")
            log.debug(initialViewControllers)
            tabBarController?.setViewControllers(initialViewControllers, animated: true)
        }
        
        changeTabItemIcons()
    }
    
    private static func changeTabItemIcons() {
        log.debug("items: \(String(describing: tabBar?.items?.count))")
        let tabBarItems = tabBar?.items?.count ?? 0
        
        let offsetIndex = (isInReadingTabEnabled) ? 0 : 1
        
        for i in 0 ..< tabBarItems {
            if let tabBarItem = tabBar?.items?[i] {
                let index = i + offsetIndex
                
                tabBarItem.imageInsets = UIEdgeInsets.init(top: 5, left: 0, bottom: -6, right: 0)
                
                let imageName = imageNames[index]
                tabBarItem.image = UIImage(named:imageName)!
                    .changeColor(UIColor.lightGray)
                    .withRenderingMode(.alwaysOriginal)
                tabBarItem.selectedImage = UIImage(named:imageName)!
                    .changeColor(UIColor.bsrBlue)
                    .withRenderingMode(.alwaysOriginal)
                
                tabBarItem.title = nil
                tabBarItem.accessibilityLabel = accessibilityLabels[index]
            }
        }
    }
    
    // Must be called before any changes are made to the base tabItems, otherwise some tabs may disappear
    private static func storeInitialTabs() {
        if initialViewControllers == nil {
            initialViewControllers = tabBarController?.viewControllers ?? []
        }
    }
}
