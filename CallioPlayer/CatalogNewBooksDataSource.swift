//
//  CatalogNewBooksDataSource.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 02.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit
import ReactiveSwift

class CatalogNewBooksDataSource: TableViewDataSource {
    typealias T = NewSearchResponse
    private var genre: GenreMapping?
    
    init(genre: GenreMapping?) {
        self.genre = genre
    }
    
    var navigationTitle: String {
        return genre?.text ?? "Toutes les nouveautés"
    }
    
    var downloadBooksCallback: (PaginationParameters) -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
        return { params in
            var p = NewSearchParameters.empty.withPagination(
                params
            )
            if let genreCode = self.genre?.code {
                p.genre = [genreCode]
            }
            return ApiHandler.bookSearch(p.asString())
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, viewController: Controller<NewSearchResponse>) -> UITableViewCell {
        let options = BuildTableViewOptions(
            books: viewController.books,
            cellIdentifier: "CatalogNewBooksViewCell",
            // currentPageNb: Int,
            readSampleCallback: viewController.readSample,
            downloadBookCallback: viewController.downloadPressed,
            bookmarkBookCallback: viewController.bookmarkPressed
        )
        
        return buildTableView(tableView, cellForRowAt: indexPath, options: options, CatalogNewBooksViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int, viewController: Controller<NewSearchResponse>) -> Int {
        return viewController.books.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, viewController: Controller<NewSearchResponse>) {
        DispatchQueue.main.async { () -> Void in
            viewController.performSegue(withIdentifier: "ShowBookFromList", sender: tableView.cellForRow(at: indexPath))
        }
    }
}
