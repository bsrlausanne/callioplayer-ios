//
//  BookTableViewController.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 02.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift

class BookTableViewController<DataSource: TableViewDataSource>: Controller<DataSource.T> {
    @IBOutlet weak var titleNavigation: UINavigationItem!

    // To be set when segueing to this view controller (~= delegate)
    var dataSource: DataSource?
    var sourceController: UIViewController?

    private var _presenter: Presenter<DataSource.T>? = Presenter<DataSource.T>()

    override var presenter: Presenter<DataSource.T>? {
        get {
            _presenter?.setView(self)
            return _presenter
        }
        set { _presenter = newValue }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (
            self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular &&
            self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular
        ) {
            tableView.rowHeight = 210.0
        } else {
            tableView.rowHeight = 125.0
            
        }
       
        log.debug("dataSource = \(String(describing: dataSource))")

        if let dataSource = self.dataSource {
            updateNavigationTitle()

            presenter?.downloadBooks(
                dataSource.downloadBooksCallback,
                prevButton: prevButton,
                nextButton: nextButton,
                pageController: pageController,
                nbPages: nbPages
            )
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        table.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowBookFromList" {
            let bookDetailsViewController = segue.destination as! BookDetailsViewController
            
            // Get the cell that generated this segue.
            if let selectedBookCell = sender as? UITableViewCell {
                let indexPath = table.indexPath(for: selectedBookCell)!
                let selectedBook = books[indexPath.row]
                bookDetailsViewController.setBook(selectedBook)
                if selectedBookCell.reuseIdentifier == "CatalogFreeBooksViewCell" {
                    bookDetailsViewController.isFreeBook = true
                }
            }
        }
    }
    
    override func prevPage() {
        super.prevPage()
        updateNavigationTitle()
    }
    
    override func nextPage() {
        super.nextPage()
        updateNavigationTitle()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.tableView(
            tableView,
            numberOfRowsInSection: section,
            viewController: self
        ) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return dataSource?.tableView(tableView, cellForRowAt: indexPath, viewController: self) ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataSource?.tableView(tableView, didSelectRowAt: indexPath, viewController: self)
    }
    
    private func updateNavigationTitle() {
        let title = dataSource?.navigationTitle ?? navigationItem.title
        // let page = presenter?.pageNb
    
        if let t = title {
            navigationItem.title = "\(t)"
        }
    }
}
