//
//  BookChapter.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 26.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct BookChapter {
    var chapterId = ""
    var title = ""
    var duration = 0
    var startAt = 0
    var audioFile = ""
    var level = 0
    
    func getAudioFilePath() -> URL {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)[0]
        
        log.debug("documentsDir = \(documentsDirectory), audioFile = \(audioFile), ")
        
        return URL(fileURLWithPath: documentsDirectory).appendingPathComponent(audioFile)
    }
    
    init () {
        
    }
    
    init (chapterDB: ChapterDB) {
        self.chapterId = chapterDB.chapterId
        self.title = chapterDB.title
        self.duration = chapterDB.duration
        self.audioFile = chapterDB.audioFile
        self.level = chapterDB.level
        self.startAt = chapterDB.startAt
    }
    
    func toDB() -> ChapterDB {
        let chapterDB = ChapterDB()
        
        chapterDB.chapterId = self.chapterId
        chapterDB.title = self.title
        chapterDB.duration = self.duration
        chapterDB.audioFile = self.audioFile
        chapterDB.level = self.level
        chapterDB.startAt = self.startAt
        
        return chapterDB
    }
}
