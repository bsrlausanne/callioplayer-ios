//
//  SingletonAVPlayer.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 15.01.21.
//  Copyright © 2021 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer

class SingletonAVPlayer : NSObject {
    static let shared = SingletonAVPlayer()
    var player : AVPlayer?
    var playerTimeObserver: Any?
}
