//
//  PaginationParameters.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 03.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct PaginationParameters {
    var resultsPerPage: Int
    var page: Int // Page value starts at 0
    
    init() {
        self.resultsPerPage = 10
        self.page = 0
    }
    
    init(pageIndex: Int, resultsPerPage: Int) {
        self.resultsPerPage = resultsPerPage
        self.page = pageIndex
    }
}
