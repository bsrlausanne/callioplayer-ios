//
//  Countable.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 03.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

protocol Countable {
    // The total number of books available
    var count: Int { get }

    // The complete or partial collection of books available
    var books: [BookModel] { get }
}
