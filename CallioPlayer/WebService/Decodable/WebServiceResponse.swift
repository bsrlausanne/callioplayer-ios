//
//  ListOfGenresResponse.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 25.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct WebServiceResponse<T: KeyDecodable>: KeyDecodable {
    var result: T
    static var codingKey: String {
        return "webservice_response"
    }
    
    struct CodingKeys: CodingKeyProtocol {
        
        var intValue: Int?
        var stringValue: String
        
        init?(intValue: Int) {
            self.intValue = intValue
            self.stringValue = "\(intValue)"
        }
        
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        
        static var result: CodingKeys {
            return CodingKeys.make(key: "result")
        }

        static func make(key: String) -> CodingKeys {
            return CodingKeys(stringValue: key)!
        }
    }
    
    init (from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let result = try container.nestedContainer(keyedBy: T.CodingKeys.self, forKey: .result)
        self.result = try result.decode(T.self, forKey: T.CodingKeys.make(key: T.codingKey))
    }
}
