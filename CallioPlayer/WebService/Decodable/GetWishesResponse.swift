//
//  GetWishesResponse.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 26.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct GetWishesResponse: KeyDecodable, Countable {
    class WishesCodingKeys: BaseCodingKeys {}
    typealias CodingKeys = WishesCodingKeys
    
    var wishes: [BookModel]
    
    var books: [BookModel] {
        return wishes
    }
    var count: Int {
        return wishes.count
    }
    
    static var codingKey: String {
        return "GetWishes"
    }
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        
        self.wishes = []
        if let limit = container.count {
            if limit > 0 {
                for _ in 0...(limit - 1) {
                    wishes.append(
                        try container.decode(BookModel.self)
                    )
                }
            }
        }
    }
}
