//
//  GetVotationsResponse.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 29.03.22.
//  Copyright © 2022 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct GetVotationsResponse: KeyDecodable, Countable {
    class VotationsCodingKeys: BaseCodingKeys {}
    typealias CodingKeys = VotationsCodingKeys
    
    var votations: [BookModel]
    
    var books: [BookModel] {
        return votations
    }
    var count: Int {
        return votations.count
    }
    
    static var codingKey: String {
        return "NextVotations"
    }
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        
        self.votations = []
        if let limit = container.count {
            if limit > 0 {
                for _ in 0...(limit - 1) {
                    votations.append(
                        try container.decode(BookModel.self)
                    )
                }
            }
        }
    }
}
