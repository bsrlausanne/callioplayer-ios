//
//  ListOfGenresResponse.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 25.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct ListOfGenresResponse: KeyDecodable {
    class GenresCodingKeys: BaseCodingKeys {}
    typealias CodingKeys = GenresCodingKeys
    
    var genres: [GenreMapping] = []
    static var codingKey: String {
        return "ListOfGenres"
    }
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        
        if let limit = container.count {
            if limit > 0 {
                for _ in 0...(limit - 1) {
                    genres.append(
                        try container.decode(GenreMapping.self)
                    )
                }
            }
        }
    }
}

struct GenreMapping: Decodable {
    var code: String
    var text: String
    
    init(code: String, text: String) {
        self.code = code
        self.text = text
    }
}
