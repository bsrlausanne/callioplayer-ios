//
//  NewSearchResponse.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 25.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct NewSearchResponse: KeyDecodable, Countable {
    class NewSearchCodingKeys: BaseCodingKeys {
        static let count = CodingKeys.make(key: "count")
    }
    typealias CodingKeys = NewSearchCodingKeys
    
    var books: [BookModel]
    var count: Int

    static var codingKey: String {
        return "NewSearch"
    }
    
    init(from decoder: Decoder) throws {
        log.debug(decoder)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.count = try container.decode(Int.self, forKey: .count)
        self.books = []
        
        var i: Int = 0
        while (true) {
            do {
                let book = try container.decode(
                    BookModel.self,
                    forKey: NewSearchCodingKeys.make(key: "\(i)")
                )

                // log.debug("book = " + String(describing: book))
                self.books.append(book)
                i += 1
            } catch let DecodingError.keyNotFound(key, ctx) {
                log.debug("key not found at index: \(key)")
                log.debug("codingPath: \(ctx.codingPath)")
                if (key.stringValue == "\(i)") {
                    break
                } else {
                    throw DecodingError.keyNotFound(key, ctx)
                }
            }
        }
        
        self.books.sort(by: { (b1, b2) in
            return b1.position < b2.position
        })
    }
}
