//
//  GetFreeBooksReponse.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 29.03.22.
//  Copyright © 2022 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct GetFreeBooksResponse: KeyDecodable, Countable {
    class FreeBooksCodingKeys: BaseCodingKeys {}
    typealias CodingKeys = FreeBooksCodingKeys
    
    var freeBooks: [BookModel]
    
    var books: [BookModel] {
        return freeBooks
    }
    var count: Int {
        return freeBooks.count
    }
    
    static var codingKey: String {
        return "GetFreeBooks"
    }
    
    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        
        self.freeBooks = []
        if let limit = container.count {
            if limit > 0 {
                for _ in 0...(limit - 1) {
                    freeBooks.append(
                        try container.decode(BookModel.self)
                    )
                }
            }
        }
    }
}
