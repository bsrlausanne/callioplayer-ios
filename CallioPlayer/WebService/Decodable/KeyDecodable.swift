//
//  KeyDecodable.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 26.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

protocol KeyDecodable: Decodable {
    associatedtype CodingKeys: CodingKeyProtocol
    
    static var codingKey: String { get }
}
