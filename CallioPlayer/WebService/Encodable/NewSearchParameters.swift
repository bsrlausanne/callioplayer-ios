//
//  SearchParameters.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 21.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

struct NewSearchParameters: Encodable {
    var queryText: String?
    var queryType: String
    var producerCode: String?
    var genre: [String]?
    var mediaType: String?
    var jeunesse: YouthOnlyFilter?
    
    var count: Int = 10
    var page: Int = 0
    
    private enum CodingKeys: String, CodingKey {
        case queryText
        case queryType
        case producerCode
        case genre
        case mediaType
        case jeunesse
        case count
        case page
    }
    
    init(
        queryText: String? = nil,
        queryType: String,
        producerCode: String? = nil,
        genre: [String]? = nil,
        mediaType: String? = nil,
        jeunesse: YouthOnlyFilter? = nil
    ) {
        self.queryText = queryText
        self.queryType = queryType
        self.producerCode = producerCode
        self.genre = genre
        self.mediaType = mediaType
        self.jeunesse = jeunesse
    }
    
    static let empty = NewSearchParameters(
        queryText: nil,
        queryType: "text",
        producerCode: nil,
        genre: nil,
        mediaType: nil,
        jeunesse: nil
    )
    
    func withPagination(_ params: PaginationParameters) -> NewSearchParameters {
        
        var s = NewSearchParameters(
            queryText: queryText,
            queryType: queryType,
            producerCode: producerCode,
            genre: genre,
            mediaType: mediaType,
            jeunesse: jeunesse
        )
        s.page = params.page
        s.count = params.resultsPerPage
        
        return s
    }
    
    func asString() -> String {
        let jsonQuery = try! JSONEncoder().encode(self)
        let stringQuery: String = String(data: jsonQuery, encoding: String.Encoding.utf8) ?? ""
        
        return stringQuery
    }
}

struct YouthOnlyFilter: Encodable {
    fileprivate var filtrer: String?
    
    init() {
        filtrer = "filtrer"
    }
}
