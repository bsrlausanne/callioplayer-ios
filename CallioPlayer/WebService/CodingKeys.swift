//
//  CodingKeys.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 25.06.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

protocol CodingKeyProtocol: CodingKey {
    var intValue: Int? { get set }
    var stringValue: String { get set }
    
    init?(intValue: Int)
    init?(stringValue: String)
    
    static func make(key: String) -> Self
}

class BaseCodingKeys: CodingKeyProtocol {
    var intValue: Int?
    var stringValue: String
    
    required init?(intValue: Int) {
        self.intValue = intValue
        self.stringValue = "\(intValue)"
    }
    
    required init?(stringValue: String) {
        self.stringValue = stringValue
    }
    
    static func make(key: String) -> Self {
        return self.init(stringValue: key)!
    }
}
