//
//  ApiHandler.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 06.06.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Alamofire


class ApiHandler {
    //DeleteWish&bookNr=" + mFiche.getNoticeNr(
    static let bsrWebservice = "https://webservice.bibliothequesonore.ch/mobile.php?"
    
    static var identifier = ""
    static var password = ""
    
    fileprivate static func loginDecorator<T>(_ callback: @escaping () -> SignalProducer<T, NSError>) -> SignalProducer<T, NSError> {
        let bundleVersion = String(describing: Bundle.main.infoDictionary!["CFBundleShortVersionString"]!);
        return SignalProducer {
            (observer: Signal<T, NSError>.Observer, _) in
            Alamofire.request(
                "\(bsrWebservice)func=Authenticate&p1=\(identifier)&p2=\(password)&p3=Callioplayer_iOS_" + bundleVersion
            ).responseJSON { response in
                callback().observe(on: UIScheduler()).startWithResult { result in
                    switch result {
                    case let .success(next):
                        observer.send(value: next)
                    case let .failure(error):
                        observer.send(error: error)
                    }
                }
            }
        }
    }
    
    fileprivate static func loginDecorator<T>(_ arg: String, callback: @escaping (String) -> SignalProducer<T, NSError>) -> SignalProducer<T, NSError> {
        let bundleVersion = String(describing: Bundle.main.infoDictionary!["CFBundleShortVersionString"]!);
        return SignalProducer {
            (observer: Signal<T, NSError>.Observer, _) in
            Alamofire.request(
                "\(bsrWebservice)func=Authenticate&p1=\(identifier)&p2=\(password)&p3=Callioplayer_iOS_" + bundleVersion
            ).responseJSON { response in
                callback(arg).observe(
                    on: UIScheduler()
                ).startWithResult { result in
                    switch result {
                    case let .success(next):
                        observer.send(value: next)
                    case let .failure(error):
                        observer.send(error: error)
                    }
                }
            }
        }
    }
    
    fileprivate static func loginDecorator<T>(_ arg: BookModel, callback: @escaping (BookModel) -> SignalProducer<T, NSError>) -> SignalProducer<T, NSError> {
        let bundleVersion = String(describing: Bundle.main.infoDictionary!["CFBundleShortVersionString"]!);
        return SignalProducer {
            (observer: Signal<T, NSError>.Observer, _) in
            Alamofire.request(
                "\(bsrWebservice)func=Authenticate&p1=\(identifier)&p2=\(password)&p3=Callioplayer_iOS_" + bundleVersion
            ).responseJSON { response in
                callback(arg).observe(on: UIScheduler()).startWithResult { result in
                    switch result {
                    case let .success(next):
                        observer.send(value: next)
                    case let .failure(error):
                        observer.send(error: error)
                    }
                }
            }
        }
    }
    
    static func addWish(_ book: BookModel) -> SignalProducer<Bool, NSError> {
        return loginDecorator(book, callback: addWishCB)
    }
    
    static func removeWish(_ book: BookModel) -> SignalProducer<Bool, NSError> {
        return loginDecorator(book, callback: removeWishCB)
    }
    
    fileprivate static func addWishCB(_ book: BookModel) -> SignalProducer<Bool, NSError> {
        return SignalProducer {
            (observer: Signal<Bool, NSError>.Observer, _) in
            Alamofire.request("\(bsrWebservice)func=AddWish&code=\(book.code)")
                .responseJSON { response in
                    print(response.request ?? "Request is empty")
                    print(response.response ?? "Response is empty")
                    print(response.result)
                    
                    if response.result.isSuccess {
                        
                        /*
                        let notification = UILocalNotification()
                        notification.alertTitle = NSLocalizedString(book.title, comment: "") + " a été ajouté de votre liste de voeux."
                        notification.alertBody = " "
                        notification.alertAction = "action"
                        notification.userInfo = ["title": 17, "UID": 1777]
                        notification.fireDate = NSDate(timeIntervalSinceNow: 1)
                        UIApplication.sharedApplication().scheduleLocalNotification(notification)
                        */
                        BookManager.setWishListDirty()
                        observer.send(value: true)
                    } else {
                        observer.send(value: false)
                    }
                    
                    observer.sendCompleted()
            }
        }
    }
    
    static func checkIfOnWishList(_ code: String, completion:@escaping (Bool) -> ())  {
         log.debug("\(code) check")
        if (BookManager.isWishListDirty) {
            self.getWishes().observe(on: UIScheduler()).startWithResult({ result in
                switch result {
                case let .success(response):
                    let wishes = response.result.wishes
                    BookManager.setWishList(wishes: wishes)
                    completion(wishes.contains(where: { b in
                        return b.code == code
                    }))
                case let .failure(error):
                    log.debug(error)
                }
            })
        } else {
            log.debug("check performed from cached values")
            completion(BookManager.isOnWishList(code))
        }
    }
    
    fileprivate static func removeWishCB(_ book: BookModel) -> SignalProducer<Bool, NSError> {
            return SignalProducer { observer, _ in
                Alamofire.request(
                    "\(bsrWebservice)func=DeleteWish&bookNr=\(book.code)"
                ).responseJSON { response in
                    if response.result.isSuccess {
                        let content = UNMutableNotificationContent()
                        content.title = book.title + " a été rétiré de votre liste de voeux."
                        content.body = " "
                        content.sound = UNNotificationSound.default
                        
                        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                        let request = UNNotificationRequest(identifier: "removeWish", content: content, trigger: trigger)
                        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                        
                        BookManager.setWishListDirty()
                        observer.send(value: true)
                    } else {
                        observer.send(value: false)
                    }
                    observer.sendCompleted()
                }
            }
        }
    
    /*
    private static func getBooksByAuthor(author: String) ->SignalProducer<[BookModel],NSError> {
        let authorUnescaped = author.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) ?? "";
        
        //https://www.bibliothequesonore.ch/rechercheBSR/auteur/Lemoine,%20Patrick
        return SigalProducer {
            Alamofire.request(.GET, "\(bsrWebservice)author=\(authorUnescaped)&number=20")
                .responseJSON { response in
                    if response.result.isSuccess {
                        if let books = (((response.result.value as? NSDictionary)?.objectForKey("result")?.objectForKey("LastBooksByType")?.objectForKey(type) as? NSArray)?.map {
                            book in
                            return BookApi(bookJson: book as! NSDictionary).toModel()
                            }) {
                            observer.sendNext(books)
                        }
                    }
                    observer.sendCompleted()
            }
        }
    }
    
    private static func getSuggestionsByBook(book: BookModel) -> SignalProducer<[BookModel], NSError> {
        let bookID : String = String(book.id);
        let url : String = "\(bsrWebservice)MoreLikeThis&p0=" + bookID;
        
        return SignalProducer {
            (observer: Observer<[BookModel], NSError>, _) in
            Alamofire.request(.GET, url).responseJSON { response in
                if response.result.isSuccess {
                    if let books = (((response.result.value as? NSDictionary)?.objectForKey("result") as? NSArray)?.map {
                        book in
                        return BookApi(bookJson: book as! NSDictionary).toModel()
                        }) {
                        observer.sendNext(books)
                    }
                }
                
                observer.sendCompleted()
            }
        }
    }
    
    */
    
    static func getListOfGenres() -> SignalProducer<WebServiceResponse<ListOfGenresResponse>, NSError> {
        return SignalProducer {
            (observer: Signal<WebServiceResponse<ListOfGenresResponse>, NSError>.Observer, _) in
            Alamofire.request(
                "\(bsrWebservice)func=ListOfGenres"
            ).responseJSON { response in
                self.handleResponse(response: response, observer: observer)
            }
        }
    }
    
    static func getNewBooks(_ query: String) -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
        return loginDecorator(query, callback: getNewBooksCB)
    }
    
    fileprivate static func getNewBooksCB(_ query: String) -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
        return SignalProducer {
            (observer: Signal<WebServiceResponse<NewSearchResponse>, NSError>.Observer, _) in
            
            let values = query.addingPercentEncoding(
                withAllowedCharacters: CharacterSet.urlQueryAllowed
                ) ?? ""

            Alamofire.request(
                "\(bsrWebservice)func=NewSearch&values=\(values)"
            ).responseJSON { response in
                self.handleResponse(response: response, observer: observer)
            }
        }
    }
    
    // todo : enlever le loginDecorator pour cette fonction, car pas besoin d'être logé pour les livres d'essais
    static func getFreeBooks() -> SignalProducer<WebServiceResponse<GetFreeBooksResponse>, NSError> {
        return loginDecorator(getFreeBooksCB)
    }
    
    fileprivate static func getFreeBooksCB() -> SignalProducer<WebServiceResponse<GetFreeBooksResponse>, NSError> {
        return SignalProducer {
            (observer: Signal<WebServiceResponse<GetFreeBooksResponse>, NSError>.Observer, _) in

            Alamofire.request(
                "\(bsrWebservice)func=GetFreeBooks"
            ).responseJSON { response in
                self.handleResponse(response: response, observer: observer)
            }
        }
    }
    
    static func getVotations() -> SignalProducer<WebServiceResponse<GetVotationsResponse>, NSError> {
        return loginDecorator(getVotationsCB)
    }
    
    fileprivate static func getVotationsCB() -> SignalProducer<WebServiceResponse<GetVotationsResponse>, NSError> {
        return SignalProducer {
            (observer: Signal<WebServiceResponse<GetVotationsResponse>, NSError>.Observer, _) in

            Alamofire.request(
                "\(bsrWebservice)func=NextVotations"
            ).responseJSON { response in
                self.handleResponse(response: response, observer: observer)
            }
        }
    }
//    static func getAudiodescription() -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
//        return loginDecorator(getAudiodescriptionCB)
//    }
//    
//    fileprivate static func getAudiodescriptionCB() -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
//        return SignalProducer {
//            (observer: Signal<WebServiceResponse<NewSearchResponse>, NSError>.Observer, _) in
//            
//            Alamofire.request(
//                "\(bsrWebservice)func=NewSearch&values=%7B%22genre%22%3A%22AU%20AUT%22%7D"
//            ).responseJSON { response in
//                self.handleResponse(response: response, observer: observer)
//            }
//        }
//    }
    
    static func getWishes() -> SignalProducer<WebServiceResponse<GetWishesResponse>, NSError> {
        return loginDecorator(getWishesCB)
    }
    
    fileprivate static func getWishesCB() -> SignalProducer<WebServiceResponse<GetWishesResponse>, NSError> {
        return SignalProducer {
            (observer: Signal<WebServiceResponse<GetWishesResponse>, NSError>.Observer, _) in

            Alamofire.request(
                "\(bsrWebservice)func=GetWishes"
            ).responseJSON { response in
                self.handleResponse(response: response, observer: observer)
            }
        }
    }
    
    static func bookSearch(_ query: String) -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
        return loginDecorator(query, callback: bookSearchCB)
    }
    
    fileprivate static func bookSearchCB(_ query: String) -> SignalProducer<WebServiceResponse<NewSearchResponse>, NSError> {
        return SignalProducer {
            (observer: Signal<WebServiceResponse<NewSearchResponse>, NSError>.Observer, _) in
            let values = query.addingPercentEncoding(
                withAllowedCharacters: CharacterSet.urlQueryAllowed
            ) ?? ""
            
            Alamofire.request(
                "\(bsrWebservice)func=NewSearch&values=\(values)"
            ).responseJSON { response in
                self.handleResponse(response: response, observer: observer)
            }
        }
    }
    
    static func tryLogin(_ identifier:String, password:String) -> SignalProducer<Bool, NSError> {
        return SignalProducer {
            (observer: Signal<Bool, NSError>.Observer, _) in
            Alamofire.request("\(bsrWebservice)func=Authenticate&p1=\(identifier)&p2=\(password)").validate(statusCode: 200..<300)
                .responseJSON { response in
                    print(response.request ?? "Empty request")  // original URL request
                    print(response.response ?? "Empty response") // URL response
                    print(response.result.isSuccess)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        
                        let response = JSON as! NSDictionary
                        
                        let address = response.object(forKey: "result") as? NSDictionary
                        let values = address?.object(forKey: "Authenticate") as? NSDictionary
                        let userName = values?.object(forKey: "displayName") as? String
                        print("JSON: \(String(describing: userName))")
                        
                        let standardUserDefaults = UserDefaults()
                        standardUserDefaults.set(userName, forKey: "displayName")
                        standardUserDefaults.synchronize()

                    }
                    
                    switch response.result {
                    case .success:
                        self.identifier = identifier
                        self.password = password
                        observer.send(value: true)
                        break;
                    case .failure:
                        observer.send(value: false)
                        break;
                    }
                    
                    observer.sendCompleted()
            }
        }
    }
    
    static func search(_ text:String) -> SignalProducer<Bool, NSError> {
        return loginDecorator(text, callback: searchCB)
    }
    
    fileprivate static func searchCB(_ text:String) -> SignalProducer<Bool, NSError> {
        return SignalProducer {
            (observer: Signal<Bool, NSError>.Observer, _) in
            Alamofire.request("\(bsrWebservice)func=Search&text=\(text)&start=\(0)&limit=\(10)")
                .responseJSON { response in
                    log.debug(response.request ?? "Empty request")  // original URL request
                    log.debug(response.response ?? "Empty response") // URL response
                    log.debug(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                    }
                    
                    observer.sendCompleted()
            }
        }
    }
    
    private static func handleResponse<T: KeyDecodable>(
        response: DataResponse<Any>,
        observer: Signal<WebServiceResponse<T>, NSError>.Observer
    ) {
        log.debug(response.request ?? "Empty request")  // original URL request
        log.debug(response.response ?? "Empty response") // URL response

        if response.result.isSuccess {
            do {
                if let data = response.data {
                    let decoder = JSONDecoder()
                    let serviceResponse = try decoder.decode(WebServiceResponse<T>.self, from: data)
                    
                    if let wishResponse = serviceResponse as? WebServiceResponse<GetWishesResponse> {
                        BookManager.setWishList(wishes: wishResponse.result.wishes)
                    }
                    observer.send(value: serviceResponse)
                }
            } catch {
                log.debug("Error after retrieving data" + String(describing: error))
                observer.send(error: error as NSError)
            }
            
            observer.sendCompleted()
        }
    }
}
