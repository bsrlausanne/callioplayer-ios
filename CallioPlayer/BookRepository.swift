//
//  BookRepository.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 27.05.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import RealmSwift
import ReactiveCocoa
import ReactiveSwift


class BookRepository {
    static func fetchAllBooks() -> SignalProducer<[BookModel], NSError> {
        return SignalProducer {
            (observer: Signal<[BookModel], NSError>.Observer, _) in
            DispatchQueue.global().async {
                let realm = try! Realm()
                let books = realm.objects(BookDB.self)
                observer.send(value: books.map {
                    return BookModel(bookDB: $0)
                })
                observer.sendCompleted()
            }
        }
    }
    
    static func saveBook(_ book: BookModel) -> SignalProducer<Bool, NSError> {
        return SignalProducer {
            (observer: Signal<Bool, NSError>.Observer, _) in
            DispatchQueue.global().async {
                let realm = try! Realm()
                
                do {
                    try realm.write {
                        realm.add(book.toDB(), update: .all)
                    }
                    
                    observer.send(value: true)
                    observer.sendCompleted()
                } catch {
                    observer.send(value: false)
                    observer.sendCompleted()
                    log.debug(error)
                }
            }
        }
    }
    
    static func removeBook(_ book: BookModel) -> SignalProducer<Bool, NSError> {
        log.debug("removebook")
        return SignalProducer {
            (observer: Signal<Bool, NSError>.Observer, _) in
            DispatchQueue.global().async {
                log.debug("tada: \(book.author)")
                log.debug("a")
                   do {
                        
                    let realm = try Realm()
                        
                    try ObjC.catchException {
                            
                            
                        let books = realm.objects(BookDB.self)
                            log.debug("b")
                            log.debug(books.first?.title)
                            log.debug("c")
                    }
                }
                catch let error {
                    print("An error ocurred: \(error)")
                }
                    //try realm.write {
                        
                        
                        //realm.delete(book.toDB())
                        
                        //observer.sendCompleted()
                        //log.debug("d")
                    //}

            }
        }
    }
    
    static func updateBookReadMark(_ book: BookModel) -> SignalProducer<Bool, NSError> {
        return SignalProducer {
            (observer: Signal<Bool, NSError>.Observer, _) in
            DispatchQueue.global().async {
                let realm = try! Realm()
                
                try! realm.write {
                    realm.add(book.toDB(), update: .all)
                    observer.send(value: true)
                    observer.sendCompleted()
                }
            }
        }
    }
}
