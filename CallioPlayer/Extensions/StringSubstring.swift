//
//  StringSubstring.swift
//  CallioPlayer
//
//  Created by Simon BSR on 18.05.17.
//  Copyright © 2017 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

extension String
{
    func substringFromIndex(_ index: Int) -> String {
        if (index < 0 || index > self.count)
        {
            print("index \(index) out of bounds")
            return ""
        }
        return String(self[self.index(self.startIndex, offsetBy: index)...])
    }
    
    func substringToIndex(_ index: Int) -> String {
        if (index < 0 || index > self.count)
        {
            print("index \(index) out of bounds")
            return ""
        }
        return String(self[self.index(self.startIndex, offsetBy: index)...])
    }
    
    func substringWithRange(_ start: Int, end: Int) -> String {
        if (start < 0 || start > self.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if end < 0 || end > self.count
        {
            print("end index \(end) out of bounds")
            return ""
        }
        let range = (self.index(self.startIndex, offsetBy: start) ..< self.index(self.startIndex, offsetBy: end))
        
        return String(self[range])
    }
    
    func substringWithRange(_ start: Int, location: Int) -> String
    {
        if (start < 0 || start > self.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if location < 0 || start + location > self.count
        {
            print("end index \(start + location) out of bounds")
            return ""
        }
        let range = (self.index(self.startIndex, offsetBy: start) ..< self.index(self.startIndex, offsetBy: start + location))
        return String(self[range])
    }
    

    func replace(_ target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
}
