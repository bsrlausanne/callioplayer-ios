//
//  DoubleFormat.swift
//  CallioPlayer
//
//  Created by Simon BSR on 20.05.17.
//  Copyright © 2017 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation

extension Double {
    func format(_ f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
