//
//  UISegmentedControl-custom.swift
//  CallioPlayer
//
//  Created by Simon BSR on 01.03.17.
//  Copyright © 2017 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

extension UISegmentedControl {
    
    func setFontSize(_ fontSize: CGFloat) {
        
        let normalTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.regular)
        ]
        
        self.setTitleTextAttributes(normalTextAttributes, for: UIControl.State())
        self.setTitleTextAttributes(normalTextAttributes, for: .highlighted)
    }
}
