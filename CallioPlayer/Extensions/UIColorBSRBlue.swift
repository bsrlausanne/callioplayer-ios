//
//  UIColorAsImage.swift
//  CallioPlayer
//
//  Created by Simon BSR on 01.03.17.
//  Copyright © 2017 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    class var bsrBlue: UIColor {
        let rgbValue = 0x195681
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
}
