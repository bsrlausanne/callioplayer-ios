//
//  ThumbnailHelper.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 05.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit

extension UIImageView {
    func assignCoverAsync(book: BookModel, width: CGFloat, height: CGFloat) -> Void {
        assignCoverAsync(
            book: book,
            width: width,
            height: height,
            insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        )
    }

    func assignCoverAsync(book: BookModel, width: CGFloat, height: CGFloat, insets: UIEdgeInsets) -> Void {
        if let cover = book.cover {
            if let url = URL(string: cover) {
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url)
                    log.debug("Image loaded for url: \(url)")
                    if let d = data {
                        DispatchQueue.main.async {
                            self.image = UIImage(data: d)?.cropped(
                                to: CGSize(
                                    width: width,
                                    height: height
                                )
                                )?.imageWithInsets(
                                    insets: insets
                            )
                        }
                    }
                }
            }
        } else {
            self.image = UIImage(named: "placeholder")?.cropped(to: CGSize(width: 195,height: 289))?.imageWithInsets(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
        }
    }
}
