//
//  UIViewHelpers.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 05.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addConstraintsWithFormat(format: String, views: [UIView]) {
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(
            NSLayoutConstraint.constraints(
                withVisualFormat: format,
                options: NSLayoutConstraint.FormatOptions(),
                metrics: nil,
                views: viewsDictionary
            )
        )
    }
    func addConstraintsWithFormat(format: String, views: UIView...) {
        self.addConstraintsWithFormat(format: format, views: views)
    }
    
    func removeAllConstraints() {
        for subview in self.subviews {
            subview.removeAllConstraints()
        }
        self.removeConstraints(self.constraints)
        
    }
}
