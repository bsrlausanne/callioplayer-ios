//
//  DownloadUtils.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 08.06.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Alamofire
import Zip
import UIKit
import SystemConfiguration

class DownloadUtils {
    typealias DownloadProgress = Double
    
    
    // dictionnary of bsr code and percentage
    
    private static var dlProgresses = [String: Int]()
    private static var bytesReserved = [String: Int64]()
    private static var progressCallback : (BookModel, DownloadProgress) -> () = { book, progress in }
    private static var request : Alamofire.Request?
    private static var requests : [String:Alamofire.Request] = [:]
    private static var unzipCallback : (Double) -> () = { arg in }
    private static var isDownloading = [String: Bool]()
    private static var resumeDataByBook = [String: Data]()
    
    static func getDlProgress() -> [String: Int] {
        return dlProgresses
    }

    static let sharedInstance: SessionManager = {
        //let configuration: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let configuration = URLSessionConfiguration.background(withIdentifier: "ch.bibliothequesonore.callioplayer.ios")

        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        let manager = SessionManager(configuration: configuration)
        
        manager.startRequestsImmediately = true
        return manager
    }()
    
    static func storeProgressCallback(_ callback: @escaping (BookModel, DownloadProgress) -> () ) {
        progressCallback = callback
    }
    
    static func storeUnzipCallback(_ callback: @escaping (Double) -> () ) {
        unzipCallback = callback
    }
    
    private static func clearDownloadData(_ code: String) {
        isDownloading.removeValue(forKey: code)
        resumeDataByBook.removeValue(forKey: code)
        bytesReserved.removeValue(forKey: code)
        resumeDataByBook.removeValue(forKey: code)
        dlProgresses.removeValue(forKey: code)
    }
    
    static func cancelDownload(_ code : String) {
        if let request = requests[code] {
            request.cancel()

            clearDownloadData(code)
        }
    }
    
    static func isBookDownloading(_ code: String) -> Bool {
        return isDownloading[code] ?? false
    }
    
    static func getRemainingFreeSpaceInBytes() -> Int64? {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let freeSize = systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber
            else {
                return nil
        }
        var totalBytesavailable = freeSize.int64Value
        for data in bytesReserved {
            totalBytesavailable -= data.1
        }
        return totalBytesavailable
    }
    
    static func errorSize() {
        let alert = UIAlertController(
            title: "Espace indisponible",
            message: "Veuillez supprimer d'anciens livres avant de télécharger.",
            preferredStyle: .alert
        )
        
        let okButton = UIAlertAction(title: "Ok", style: .default)
        
        alert.addAction(okButton)
        alert.show((UIApplication.shared.keyWindow?.rootViewController)!, sender: self)
    }
    
    /*
    static func backgroundDownload(_ book: BookModel) -> SignalProducer<Bool, NSError> {
         return SignalProducer {
            (observer : Signal<Bool, NSError>.Observer, _) in if(!book.downloadUri.isEmpty) {
                
                let configuration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier("bgSessionConfiguration")
                let backgroundSession = NSURLSession(configuration: configuration)
                let task = backgroundSession.downloadTaskWithURL(NSURL(string: book.downloadUri)!)

                task.resume()
                observer.sendNext(true)
                observer.sendCompleted()
            } else {
                observer.sendNext(false)
                observer.sendCompleted()
            }
        }
    }
    */
    
    static func downloadBook(_ book: BookModel) -> SignalProducer<Bool, NSError> {
        
        if let check = isDownloading[book.code]  {
            
            let alert = UIAlertController(
                title: "Livre en téléchargement",
                message: "\(book.title) est déjà en train d'être téléchargé.",
                preferredStyle: .alert
            )
            
            let okButton = UIAlertAction(title: "Ok", style: .default)
            
            alert.addAction(okButton)
            alert.show((UIApplication.shared.keyWindow?.rootViewController)!, sender: self)
            
            if check {
                return SignalProducer {
                    (observer: Signal<Bool, NSError>.Observer, _) in
                    observer.send(value: false)
                    observer.sendCompleted()
                }
            }
        }
        return SignalProducer {
            (observer: Signal<Bool, NSError>.Observer, _) in
            
            let downloadUri = book.downloadUri ?? ""
            if !downloadUri.isEmpty {
                isDownloading[book.code] =  true;
                progressCallback(book, 0)
                
                if let resumeData = resumeDataByBook[String(book.code)] {
                    print(resumeData)
                   // self.request = sharedInstance.download(resumeData)
                } else {
                    self.request = sharedInstance.download(
                        downloadUri,
                        method: .get
                    ) { temporaryUrl, response in // download file destination
                        let directoryURL = FileManager.default.urls(
                            for: .documentDirectory,
                            in: .userDomainMask
                        )[0]
                        
                        var fileName: String
                        if let suggestedName = response.suggestedFilename {
                            fileName = suggestedName
                        } else {
                            fileName = downloadUri.filter { (c) -> Bool in
                                let string = "\(c)"
                                let pattern = "\\b[a-zA-Z0-9._-]\\b"
                                let regexp = try? NSRegularExpression(pattern: pattern)
                                let range = NSMakeRange(0, string.count)
                                
                                return regexp?.firstMatch(in: string, options: [], range: range) != nil
                            }
                        }
                        let finalPath = directoryURL.appendingPathComponent(fileName)
                        
                        log.debug("finalPath = \(finalPath) with filename = \(fileName)")
                        return (finalPath, [])
                    }.downloadProgress { progress in
                        DispatchQueue.main.async() {
                            progressCallback(book, progress.fractionCompleted)
                            
                            let totalBytesExpectedToRead = progress.totalUnitCount
                            if bytesReserved[book.code] == nil {
                                let remainingFreeSpaceInBytes = self.getRemainingFreeSpaceInBytes();
                                // Not enough space to downlaod and unzip. i.e. 2.1
                                if Double(remainingFreeSpaceInBytes!) < Double(totalBytesExpectedToRead) * 2.1 {
                                    self.request?.cancel()
                                    errorSize();
                                } else {
                                    bytesReserved[book.code] = totalBytesExpectedToRead
                                }
                            }
                        }
                    }.response { response in
                        if response.error != nil {
                            // Si il y a eu une erreur
                            // Store the resume data
                            // resumeDataByBook[String(book.code)] = response.resumeData
                            progressCallback(book, 100)
                            observer.send(value: false)
                            observer.sendCompleted()
                            isDownloading.removeValue(forKey: book.code)
                        } else {
                            dlProgresses.removeValue(forKey: book.code)
                            
                            
                            if let destinationURL = response.destinationURL {
                                log.debug("destinationURL = \(destinationURL)")
                                let filePathUnzip = unzipFile(destinationURL)
                                bytesReserved[book.code] = nil
                                BookManager.saveNewBook(filePathUnzip.lastPathComponent, book: book)
                                progressCallback(book, 100)
                                isDownloading.removeValue(forKey: book.code)
                                observer.send(value: true)
                            } else {
                                let error = NSError(
                                    domain: "Missing destination url for unzipping file.",
                                    code: 200,
                                    userInfo: nil
                                )
                                observer.send(error: error)
                            }
                            
                            observer.sendCompleted()
                        }
                    }
                }
                // register request 
                requests[book.code] = request
                
            } else {
                observer.send(value: false)
                observer.sendCompleted()
            }
        }
    }
    
    fileprivate static func unzipFile(_ file: URL) -> URL {
        do {
            let unzipDirectory = try Zip.quickUnzipFile(file)
            let fileManager = FileManager.default
            
            do {
                try fileManager.removeItem(at: file)
            }
            catch let error as NSError {
                print(error)
            }
            return unzipDirectory
        } catch {
            return URL(fileURLWithPath: "")
        }
    }
}
