//
//  File.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 08.06.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import AVFoundation


class AudioPlayerUnit: NSObject {
    var player: AVPlayer?
    
    let kSamplesRate = 22050;
    let kSamplesLength = 4096;
    let kModifiedSamplesLength = 2048;
    let kChannelCount = 1;
    
    func pause() {
        player?.pause()
    }
    
    func play() {
        player?.play()
    }
    
    func stop() {
        player?.pause()
        player = nil
    }
    
    func readRemoteAudioSample(_ url: String) {
        log.debug("will try to play url = \(url)")
        if let player = self.player {
            // https://stackoverflow.com/a/9288642
            if player.rate != 0 && player.error == nil {
                player.pause()
            }
        }

        if let url = URL(string: url) {
            self.player = AVPlayer(url: url)
        } else {
            log.debug("pas d'extrait")
        }

        self.play()
    }
}
