//
//  CatalogWishListDataSource.swift
//  CallioPlayer
//
//  Created by Florian Alonso on 04.07.19.
//  Copyright © 2019 Bibliothèque Sonore Romande. All rights reserved.
//

import UIKit
import ReactiveSwift

class CatalogWishListDataSource: TableViewDataSource {
    typealias T = GetWishesResponse
    
    var navigationTitle: String = "Liste de voeux"
    
    var downloadBooksCallback: (PaginationParameters) -> SignalProducer<WebServiceResponse<GetWishesResponse>, NSError> {
        return { _ in
            return ApiHandler.getWishes()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, viewController: Controller<GetWishesResponse>) -> UITableViewCell {
        let options = BuildTableViewOptions(
            books: viewController.books,
            cellIdentifier: "CatalogWishListViewCell",
            readSampleCallback: viewController.readSample,
            downloadBookCallback: viewController.downloadPressed,
            bookmarkBookCallback: viewController.bookmarkPressed
        )
        
        return buildTableView(tableView, cellForRowAt: indexPath, options: options, CatalogWishListViewCell.self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int, viewController: Controller<GetWishesResponse>) -> Int {
        return viewController.books.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath, viewController: Controller<GetWishesResponse>) {
        DispatchQueue.main.async { () -> Void in
            viewController.performSegue(withIdentifier: "ShowBookFromList", sender: tableView.cellForRow(at: indexPath))
        }
    }
}
