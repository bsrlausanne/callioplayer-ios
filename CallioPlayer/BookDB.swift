//
//  BookDB.swift
//  CallioPlayer
//
//  Created by Simon Schulé on 27.05.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import RealmSwift


class BookDB: Object {
    @objc dynamic var id = ""
    @objc dynamic var code = ""
    @objc dynamic var title = ""
    @objc dynamic var author = ""
    @objc dynamic var cover = ""
    @objc dynamic var editor = ""
    @objc dynamic var duration = 0
    @objc dynamic var path = ""
    @objc dynamic var reader = ""
    @objc dynamic var media = ""
    @objc dynamic var currentRate: Float = 1.0
    @objc dynamic var timeSeek: Double = 30
    
    @objc dynamic var currentReading: CurrentReadingDB? = CurrentReadingDB()
    let chapters = List<ChapterDB>()
    let bookmarks = List<BookmarkDB>()

    
    override class func primaryKey() -> String? {
        return "id"
    }
}

class ChapterDB: Object {
    @objc dynamic var chapterId = ""
    @objc dynamic var title = ""
    @objc dynamic var duration = 0
    @objc dynamic var audioFile = ""
    @objc dynamic var level = 0
    @objc dynamic var startAt = 0
}

class CurrentReadingDB: Object {
    @objc dynamic var chapterId = ""
    @objc dynamic var isReading = false
    @objc dynamic var startPoint = 0
}

class BookmarkDB: Object {
    @objc dynamic var chapterId = ""
    @objc dynamic var title = ""
    @objc dynamic var duration = 0
    @objc dynamic var startPoint = 0
}
