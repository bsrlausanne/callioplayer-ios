//
//  LogUtils.swift
//  CallioPlayer
//
//  Created by Stéphane Maillard on 24.05.16.
//  Copyright © 2016 Bibliothèque Sonore Romande. All rights reserved.
//

import Foundation
import XCGLogger

func INIT_XCGLOGGER() -> XCGLogger {
    // Setup XCGLogger
    let log = XCGLogger.default
    
    let logPath: URL = appDelegate.cacheDirectory.appendingPathComponent("XCGLogger_Log.txt")
    log.setup(level: .debug, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: logPath)
    
    return log
}

func ENTRY_LOG(_ functionName: StaticString = #function, fileName: StaticString = #file, lineNumber: Int = #line) -> Void {
    log.debug("ENTRY", functionName:functionName, fileName:fileName, lineNumber:lineNumber)
}

func EXIT_LOG(_ functionName: StaticString = #function, fileName: StaticString = #file, lineNumber: Int = #line) -> Void {
    log.debug("EXIT", functionName:functionName, fileName:fileName, lineNumber:lineNumber)
}
