//
//  Controller.swift
//  CallioPlayer
//
//  Created by Faton Ramadani on 17.02.17.
//  Copyright © 2017 Bibliothèque Sonore Romande. All rights reserved.
//
import UIKit
import ReactiveCocoa
import ReactiveSwift
import AVFoundation
import UserNotifications

class Controller<T: KeyDecodable & Countable>: UITableViewController {
    
    @IBOutlet var table: UITableView!

    @IBOutlet weak var pageController: UIView!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nbPages: UILabel!
    
    var books: [BookModel] = []
    var presenter: Presenter<T>! // This is initialized in the Configurator files
    var audioPlayer = AudioPlayerUnit()
    var currentMP3File = ""
    var currentButton: UIBarButtonItem?
    var sampleRunning = false

    override func viewDidLoad() {
        prevButton.isHidden = true
        
         //problème d'unwraping de traitCollection à régler ici
        if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular &&
            self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular) {
            tableView.rowHeight = 210.0
        } else{
            tableView.rowHeight = 85.0
        }

        log.debug("Attaching targets")
        prevButton.addTarget(self, action: #selector(Controller.prevPage), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(Controller.nextPage), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(audioPlayerDidFinishPlaying),
            name: .AVPlayerItemDidPlayToEndTime,
            object: nil
        )
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        maybePauseSample()
    }
    
    // displays the previous books, cached.
    @objc func prevPage() {
        maybePauseSample()
        presenter.showPrev(
            nextButton: nextButton,
            prevButton: prevButton,
            pageController: pageController,
            postLoadCallback: self.scrollToFirstRow,
            nbPages: nbPages
        )
    }
    
    @objc func nextPage() {
        maybePauseSample()
        presenter.showNext(
            nextButton: nextButton,
            prevButton: prevButton,
            pageController: pageController,
            postLoadCallback: self.scrollToFirstRow,
            nbPages: nbPages
        )
    }

    func scrollToFirstRow() {
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        let triggerTime = (Int64(NSEC_PER_SEC) / 2)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: { () -> Void in
            self.goToTop()
        })
    }
    
    func goToTop(){
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: tableView.cellForRow(at: IndexPath(row: 0, section: 0)));
    }

    func getPresenter() -> Presenter<T> {
        return presenter
    }
    
    func displayBooks(_ books: [BookModel]){
        self.books = books
        table.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(books.count == 0){
            return 1
        } else{
            return books.count
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 100))
        return footerView
    }
    

    
    func downloadBook(_ book: BookModel) {
        // let book = books[sender.tag]
        
        let content = UNMutableNotificationContent()
        content.title = "Téléchargement de " + book.title
        content.body = " "
        content.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: "localNotification", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
            
            if(!reachability.isReachableViaWiFi()) {
                let alertController = UIAlertController(title: "Attention", message: "Vous êtes connecté à un réseau de téléphonie mobile. Télécharger quand même?", preferredStyle: .alert)
                
                let oui = UIAlertAction(title: "Oui", style: .default, handler: { (UIAlertAction) -> Void in
                    DownloadUtils.downloadBook(book).observe(
                        on: UIScheduler()
                    ).startWithResult { result in
                        switch result {
                        case let .success(next):
                            print("DownloadSuccess : \(next)")
                            
                            if(next) {
                                let content = UNMutableNotificationContent()
                                content.title = "Livre prêt : " + book.title
                                content.body = " "
                                content.sound = UNNotificationSound.default
                                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                                let request = UNNotificationRequest(identifier: "localNotification", content: content, trigger: trigger)
                                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                            }
                        case let .failure(error):
                            print("Download failed: \(error)")
                        }
                    }
                    
                })
                let non = UIAlertAction(title: "Non", style: .default, handler: nil)
                alertController.addAction(oui)
                alertController.addAction(non)
                
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                DownloadUtils.downloadBook(book).observe(
                    on: UIScheduler()
                ).startWithResult { result in
                    switch result {
                    case let .success(next):
                        print("DownloadSuccess hugo : \(next)")
                        
                        if(next) {
                            let content = UNMutableNotificationContent()
                            content.title = "Livre prêt : " + book.title
                            content.body = " "
                            content.sound = UNNotificationSound.default
                            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                            let request = UNNotificationRequest(identifier: "localNotification", content: content, trigger: trigger)
                            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                        }
                    case let .failure(error):
                        print("Download failed: \(error)")
                    }
                }
            }
            
        } catch {
            
            //print("Unable to create Reachability")
        }
    }
    
    @objc func readSample(_ sender: UIBarButtonItem) {
        let book = books[sender.tag]
        let newMP3File = book.sampleMp3Uri ?? ""
        
        log.debug("sampleRunning = \(sampleRunning), newMp3File = \(newMP3File), currentMp3 = \(currentMP3File)")
        
        if sampleRunning {
            currentButton?.image = UIImage(named: "ic_play_arrow_36pt")
            currentButton?.accessibilityLabel = "écouter un extrait de \(book.title)"
            currentButton?.tintColor = UIColor.white
            
            if currentMP3File != newMP3File {
                audioPlayer.pause()
                audioPlayer.readRemoteAudioSample(newMP3File)
                sender.image = UIImage(named: "ic_pause_white_36pt")
                sender.accessibilityLabel = "pause"
                sender.tintColor = UIColor.white
                sampleRunning = true
            } else {
                audioPlayer.pause()
                sampleRunning = false
            }
            
            currentMP3File = newMP3File
            currentButton = sender
            
        } else {
            sampleRunning = true
            currentMP3File = newMP3File
            currentButton = sender
            
            sender.image = UIImage(named: "ic_pause_white_36pt")
            sender.accessibilityLabel = "pause"
            sender.tintColor = UIColor.white
            
            audioPlayer.readRemoteAudioSample(newMP3File)
        }
        
        UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: sender);
    }
    
    private func maybePauseSample() {
        if sampleRunning {
            audioPlayer.pause()
            currentButton?.image = UIImage(named: "ic_play_arrow_36pt")
            sampleRunning = false
        }
    }
    
    @objc private func audioPlayerDidFinishPlaying(notification: NSNotification) {
        self.audioPlayer.stop()
        currentButton?.image = UIImage(named: "ic_play_arrow_36pt")
        sampleRunning = false
    }
    
    func buildTableView<T: BookListViewCell>(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath,
        cellIdentifier: String,
        _ workaround: T.Type
    ) -> UITableViewCell {
        return buildTableView(
            tableView,
            cellForRowAt: indexPath,
            cellIdentifier: cellIdentifier,
            navigationTitle: nil,
            workaround
        )
    }
    
    // FIXME: Button labels seem to not refresh properly (e.g. download disabled after download is cancelled)
    func buildTableView<T: BookListViewCell>(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath,
        cellIdentifier: String,
        navigationTitle: String?,
        _ workaround: T.Type
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! T
        
        if books.count > 0 {
            self.navigationItem.title = navigationTitle ?? "Résultats, page \(self.presenter.pageNb + 1)"
            let book = books[indexPath.row]
            
            cell.authorLabel.text = book.author
            cell.titleLabel.text = book.title
            
            
            if let m = book.media {
                cell.durationLabel.text = m.replace("00:", withString: "").replace(":", withString: "h. ") + "min."
            } else {
                cell.durationLabel.text = "-"
            }
            
            ApiHandler.checkIfOnWishList(book.code) { success in
                if success {
                    cell.bookmarkButton.image = UIImage(named: "ic_star_white")
                    cell.bookmarkButton.accessibilityLabel = "Retirer de la liste de voeux."
                } else {
                    cell.bookmarkButton.image = UIImage(named: "ic_star_border_white")
                    cell.bookmarkButton.accessibilityLabel = "Ajouter à la liste de voeux."
                }
            }
            
            var alreadyThere = false
            var accessibleText = "télécharger \(book.title)"
            
            log.debug("book \(book.code) is in library ? \(BookManager.bookInLibrary(book.code))")
            if BookManager.bookInLibrary(book.code) {
                alreadyThere = true
                accessibleText = "livre déjà sur l'appareil"
            }
            if DownloadUtils.isBookDownloading(book.code) {
                alreadyThere = true
                accessibleText = "en cours de téléchargement"
            }
            cell.downloadButton.isEnabled = !alreadyThere
            
            cell.downloadButton.target = self;
            cell.downloadButton.action = #selector(self.downloadPressed)
            cell.downloadButton.tag = Int(indexPath.row)
            cell.downloadButton.accessibilityLabel = accessibleText
            
            cell.playButton.target = self;
            cell.playButton.action = #selector(readSample)
            cell.playButton.tag = Int(indexPath.row)
            cell.playButton.accessibilityLabel = "écouter un extrait de \(book.title)"
            cell.bookmarkButton.target = self;
            cell.bookmarkButton.action = #selector(bookmarkPressed)
            cell.bookmarkButton?.tag = Int(indexPath.row)
            
            return cell
        } else{
            cell.authorLabel.text = ""
            cell.titleLabel.text = "Aucun livre trouvé."
            cell.durationLabel.text = "Elargissez votre recherche."
            cell.isUserInteractionEnabled = false
            cell.toolbar.isHidden = true
            pageController.isHidden = true

            return cell
            
        }
    }
    
    @objc func downloadPressed(_ sender: UIBarButtonItem) {
        let book = books[sender.tag]
        sender.isEnabled = false
        downloadBook(book)
    }
    
    @objc func bookmarkPressed(_ sender: UIBarButtonItem) {
        let book = books[sender.tag]
        log.debug("bookmark: \(book)")
        ApiHandler.checkIfOnWishList(book.code) { success in
            if success {
                sender.image = UIImage(named: "ic_star_border_white")
                self.presenter.removeFromWishList(book,sender: sender)
                log.debug("remove: \(book.code)")
            } else {
                sender.image = UIImage(named: "ic_star_white")
                log.debug("added: \(book.code)")
                self.presenter.addToWishList(book,sender: sender)
            }
        }
        
        
    }
    
}
